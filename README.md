# Blog Stat Viewer

This is a home-rolled blog/static site statistics generator and viewer meant to allow for a modicum of determining which
posts are getting the most traffic, most frequent user stats, where referrals are coming from, etc. but solely based on
looking latent access logs of the web server. While I'm writing this for my own purposes since it could be useful for
others I'm open sourcing it and making it publicly available with
an [AGPLv3 open source license](https://www.gnu.org/licenses/agpl-3.0.html).

## Building

To build you'll need a JDK 11 or higher installed on your system. I use OpenJDK so this is what it was tested on. You'll
also need a git client if you are checking out the source directly rather than downloading a zip file of it.

To go from a clean checkout to a self contained Java Archive file:

```bash
git clone https://gitlab.com/HankG/blogstatviewer.git
cd blogstatviewer
./gradlew shadowJar
```

The executable will then be in the `./build/libs` folder for use.

## Usage

To use the program invoke from the command line using `java -jar` such as below where we use the `--help` flag to list
the help:

```bash
$ java -jar blogstatviewer-1.0-SNAPSHOT-all.jar --help

Usage: run-analysis [OPTIONS] SETTINGSFILE

Options:
  -b, --batch INT      Size of each batch insert to database (default = 100)
  -i, --ingest         Whether to try to ingest new log data
  -s, --summarize      Whether to try to ingest new log data
  --find-badip         Look for potentially bad IPs in the data
  --list-ip TEXT       List all entries for the IP address
  -f, --apply-filters  Applies the current filter configuration to the
                       existing dataset, toggling hidden field accordingly not
                       deleting data
  -h, --help           Show this message and exit

Arguments:
  SETTINGSFILE  Location of run settings configuration
```

The command line arguments are instructions are listed below as printed by the help file. Some example usages are:

#### Ingesting new entries from the log files and outputing new summaries

```bash
java -jar blogstatviewer-1.0-SNAPSHOT-all.jar --ingest --summarize settings.json
```

#### Running analysis to see if there are any potentially new bad IPs to flag:

```bash
java -jar blogstatviewer-1.0-SNAPSHOT-all.jar --find-badip settings.json
```

#### Applying updated filter settings to existing dataset:

```bash
java -jar blogstatviewer-1.0-SNAPSHOT-all.jar --apply-filters settings.json
```

#### Specifying the logging date format:

```bash
java -Dorg.slf4j.simpleLogger.showDateTime=true \
     -Dorg.slf4j.simpleLogger.dateTimeFormat="YYYY-MM-dd'T'HH:mm:ss" \
     -jar blogstatviewer-1.0-SNAPSHOT-all.jar \
     --ingest \
     --summarize \
     settings.json
```
### Execution details

The user that runs this tool simply needs to have read access to the folder in question. It is looking for Nginx
standard access log files. In a default configured system on Linux it would be writing to the
`/var/log/nginx` folder. Any user in the administrators group should be able to access this folder without any
permissions changes or the need to execute this with `sudo`.

When Ingestion Mode is run the application traverses all sub-directories of the specified `logFolderPath` in the run
configuration. Any file that begins with `access` but does not end with `.gz` will be processed. Each log line is
SHA-256 hashed which is used to prevent duplicate entries in the database.

### Automation Details

For my own configuration I have a non-root user in a group with read only permissions to the `/var/log/nginx` folder. In
that user's home folder I have a subfolder for the tool with the following directory structure:

* **bin**: where JAR executable file is located
* **config**: where configuration files are stored
* **data**: where the file database with all historical data is stored
* **log**: Where all log files from the runs are stored
* **results**: Where the summary results are stored

In that user's home directory I have a`analysis.sh` shell script configured to execute the application with ingest and
summarize with the blog format date/time settings I like:

```bash
#!/bin/bash
java -Dorg.slf4j.simpleLogger.showDateTime=true \
     -Dorg.slf4j.simpleLogger.dateTimeFormat="YYYY-MM-dd'T'HH:mm:ss" \
     -jar ~/statstool/bin/blogstatviewer-1.0-SNAPSHOT-all.jar \
     --ingest \
     --summarize \
     ~/statstool/config/settings.json
```

For execution at the top and bottom of every hour (so every thirty minutes) I have that user's crontab setup as:

```bash
*/30 * * * * ./analysis.sh 2>> /home/<USERNAME>/statstool/log/output.log
```

This appends to the log so I can see a history of how the insertions and execution went.


## Configuration

All program configuration is done through settings JSON files. The root file is the a RunSettings file an example of
which one can see
[here](https://gitlab.com/HankG/blogstatviewer/-/blob/develop/src/commonTest/resources/runSettings.json).

Along with top level configurations in that file there are specific files it references for other configurations. These
include:

* **Filter Settings**: which references the settings used when configuring filter values. These are things like the list
  of known bots, bad IP addresses, and desired blog post years. An example can be
  seen [here](https://gitlab.com/HankG/blogstatviewer/-/blob/develop/src/commonTest/resources/filterConfig.json)
* **Referrer Mappings**: This captures common information about known external referrers like Facebook, Google, etc. The
  example file
  [here](https://gitlab.com/HankG/blogstatviewer/-/blob/develop/src/commonTest/resources/referrerMappingSettings.json)
  shows the referrers that I've personally encountered. If a referrer comes up which isn't shown then it just puts the
  raw value in the summary data.
* **Internal Mappings**: This captures common information about known site referrers from inside the website. The
  example
  [here](https://gitlab.com/HankG/blogstatviewer/-/blob/develop/src/commonTest/resources/internalMappingSettings.json)
  shows the configuration for my personal blog.
  

  