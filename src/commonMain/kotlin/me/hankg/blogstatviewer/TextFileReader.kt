package me.hankg.blogstatviewer

expect object TextFileReader {
    fun readAllLines(filePath:String):Collection<String>
}