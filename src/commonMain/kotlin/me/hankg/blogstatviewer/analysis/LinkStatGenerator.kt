package me.hankg.blogstatviewer.analysis

import kotlinx.datetime.*

import me.hankg.blogstatviewer.model.*
import me.hankg.blogstatviewer.parsers.InternalReferrerParser
import me.hankg.blogstatviewer.parsers.PlatformParser
import me.hankg.blogstatviewer.parsers.ReferrerParser

class LinkStatGenerator(
    externalReferrerSettings: ReferrerMappingSettings,
    internalReferrerSettings: InternalReferrerSettings
) {
    private val externalReferrerParser = ReferrerParser(externalReferrerSettings)
    private val internalReferrerParser = InternalReferrerParser(internalReferrerSettings)
    private val platformParser = PlatformParser()

    fun generate(entries: Collection<AccessLogEntry>): Map<String, LinkStats> {
        val stats = mutableMapOf<String, LinkStats>()
        entries.forEach {
            val trimmedPath = it.requestPath.trim()
            val normalizedRequestPath = if(trimmedPath.endsWith('/')) trimmedPath else "$trimmedPath/"
            val linkStats = stats.getOrPut(normalizedRequestPath) { LinkStats(link = normalizedRequestPath) }
            linkStats.viewCount += 1

            val ipCount = linkStats.ipStats.getOrElse(it.ipAddress) { 0 }
            linkStats.ipStats[it.ipAddress] = ipCount + 1

            val internalReferrer = internalReferrerParser.convertToType(it)
            if (internalReferrer.isNotEmpty()) {
                val internalReferrerCount = linkStats.internalReferrers.getOrElse(internalReferrer) { 0 }
                linkStats.internalReferrers[internalReferrer] = internalReferrerCount + 1
            }

            val externalReferrer = externalReferrerParser.convertToType(it)
            if (externalReferrer.isKnownMapping && internalReferrer.isEmpty()) {
                val externalReferrerCount =
                    linkStats.externalReferrers.getOrElse(externalReferrer.calculatedValue) { 0 }
                linkStats.externalReferrers[externalReferrer.calculatedValue] = externalReferrerCount + 1
            }

            val platformData = platformParser.getPlatform(it)
            val platformCount = linkStats.platformData.getOrElse(platformData) { 0 }
            linkStats.platformData[platformData] = platformCount + 1
        }
        return stats
    }

    fun generateByTimeBuckets(entries: Collection<AccessLogEntry>, binSize: TimeBinSize): List<LinkStatsDateBin> {
        val binnedData = mutableMapOf<LocalDate, MutableList<AccessLogEntry>>()

        entries.sortedBy { it.accessTimeMillis }
            .forEach {
                val accessDate = Instant.fromEpochMilliseconds(it.accessTimeMillis)
                    .toLocalDateTime(TimeZone.of("UTC"))
                    .date
                val indexDate = when(binSize){
                    TimeBinSize.Day -> accessDate
                    TimeBinSize.Month -> LocalDate(accessDate.year, accessDate.month, 1)
                    TimeBinSize.Year -> LocalDate(accessDate.year, 1, 1)
                }
                binnedData.getOrPut(indexDate) { mutableListOf() }.add(it)
            }

        return binnedData.map { (date, entries) ->
            LinkStatsDateBin(date, generate(entries))
        }.toList()
    }
}