package me.hankg.blogstatviewer.analysis

import me.hankg.blogstatviewer.model.AccessLogEntry

class SuspiciousIpAnalysis(private val windowDurationMillis: Long = 10L, private val maxPerWindow: Int = 20) {
    fun findIps(entries: Collection<AccessLogEntry>): List<String> {
        val badIps = mutableListOf<String>()
        val ipData = mutableMapOf<String, MutableList<Long>>()

        entries.sortedBy { it.accessTimeMillis }.forEach {
            val ip = it.ipAddress
            val time = it.accessTimeMillis
            ipData.getOrPut(ip) { mutableListOf() }.add(time)
        }

        ipData.filter { it.value.size > maxPerWindow }.forEach { (ip, times) ->
            if ((times.last() - times.first() < windowDurationMillis)) {
                badIps.add(ip)
            } else {
                for (t in 0 until times.size - maxPerWindow - 1) {
                    val dt = times[t + maxPerWindow] - times[t]
                    if (dt < windowDurationMillis) {
                        badIps.add(ip)
                        break
                    }
                }
            }
        }

        return badIps
    }
}