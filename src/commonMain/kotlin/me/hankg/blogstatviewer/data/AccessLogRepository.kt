package me.hankg.blogstatviewer.data

import me.hankg.blogstatviewer.model.AccessLogEntry

interface AccessLogRepository {

    fun countAll(): Long

    fun findAll(excludeHidden: Boolean): Collection<AccessLogEntry>

    fun findByIp(ip: String): Collection<AccessLogEntry>

    fun insert(entry: AccessLogEntry): Boolean

    fun insertMany(entries: Collection<AccessLogEntry>): Boolean

    fun update(entry: AccessLogEntry): Boolean

}