package me.hankg.blogstatviewer.data

expect object AccessLogRepositoryFactory {

    fun getSqliteDbFromFilePath(filePath: String): AccessLogRepository

    fun getSqliteDatabase(connectionString: String): AccessLogRepository

    fun generateSqliteConnectionString(filePathtoDb: String): String
}