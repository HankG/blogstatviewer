package me.hankg.blogstatviewer.filtering

import me.hankg.blogstatviewer.model.AccessLogEntry

fun AccessLogEntry.checkIsOkay(criteria: List<Filter<AccessLogEntry>>): Boolean {
    if (criteria.isEmpty()) return true
    criteria.forEach {
        if (!it.checkIsOkay(this)) {
            return false
        }
    }

    return true
}