package me.hankg.blogstatviewer.filtering

import me.hankg.blogstatviewer.model.AccessLogEntry

class AllowedResponseCodeFilter(private val responseCodes: List<Int> = listOf()) : Filter<AccessLogEntry> {
    override fun checkIsOkay(item: AccessLogEntry): Boolean {
        if (responseCodes.isEmpty()) return true
        return responseCodes.contains(item.responseCode)
    }
}