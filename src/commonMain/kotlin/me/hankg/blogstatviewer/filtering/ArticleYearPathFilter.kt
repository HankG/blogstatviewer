package me.hankg.blogstatviewer.filtering

import me.hankg.blogstatviewer.model.AccessLogEntry

class ArticleYearPathFilter(startYear: Int = 2000, stopYear: Int = 2100) : Filter<AccessLogEntry> {
    private val actualFilter: Filter<AccessLogEntry>

    init {
        val years: List<String> = (startYear..stopYear).map { "/$it" }.toList()
        actualFilter = RelPathStartFilter(years)
    }

    override fun checkIsOkay(item: AccessLogEntry): Boolean = actualFilter.checkIsOkay(item)

}