package me.hankg.blogstatviewer.filtering

import me.hankg.blogstatviewer.model.AccessLogEntry
import me.hankg.blogstatviewer.model.BotSettings

class BotFilter(private val botNameFragments: List<BotSettings> = listOf()) : Filter<AccessLogEntry> {
    override fun checkIsOkay(item: AccessLogEntry): Boolean {
        if (botNameFragments.isEmpty()) return true
        botNameFragments.forEach {
            if (it.forReferrer && item.referrer.contains(it.subString, ignoreCase = true)) {
                return false
            }

            if (it.forAgent && item.userAgent.contains(it.subString, ignoreCase = true)
            ) {
                return false
            }
        }
        return true
    }
}