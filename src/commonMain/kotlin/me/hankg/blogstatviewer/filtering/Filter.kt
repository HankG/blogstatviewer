package me.hankg.blogstatviewer.filtering

/**
 * Interface for checking if something should get through a filtering process
 */
interface Filter<T> {

    /**
     * Evaluates whether an item is okay to pass through the filter.
     */
    fun checkIsOkay(item: T): Boolean
}