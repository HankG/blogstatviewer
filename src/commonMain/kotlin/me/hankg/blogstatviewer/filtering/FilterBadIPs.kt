package me.hankg.blogstatviewer.filtering

import me.hankg.blogstatviewer.model.AccessLogEntry

class FilterBadIPs(private val ipList: List<String> = listOf()) : Filter<AccessLogEntry> {
    override fun checkIsOkay(item: AccessLogEntry): Boolean = !ipList.contains(item.ipAddress)
}