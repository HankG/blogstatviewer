package me.hankg.blogstatviewer.filtering

import me.hankg.blogstatviewer.model.AccessLogEntry

class RelPathStartFilter(private val matches:List<String> = listOf()): Filter<AccessLogEntry> {
    override fun checkIsOkay(item: AccessLogEntry): Boolean {
        if (matches.isEmpty()) return true
        matches.forEach {
            if (item.requestPath.startsWith(it)) {
                return true
            }
        }
        return false
    }
}