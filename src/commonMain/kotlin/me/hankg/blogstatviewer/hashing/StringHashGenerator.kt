package me.hankg.blogstatviewer.hashing

expect object StringHashGenerator {
    fun hashString(string: String): String
}