package me.hankg.blogstatviewer.model

/**
 * Models the fields of an access log entry that is tracked
 */
data class AccessLogEntry(
    val ipAddress: String = NO_IP,
    val accessTimeMillis: Long = NO_TIMESTAMP,
    val requestPath: String = NO_PATH,
    val responseCode: Int = NO_RESPONSE,
    val bytesSent: Long = 0,
    val referrer: String = NO_REFERRER,
    val userAgent: String = NO_AGENT,
    val hidden: Boolean = DEFAULT_HIDDEN_VALUE,
    val textHash: String = NO_HASH,
) {
    val purePath = purePathGen()

    companion object {
        const val DEFAULT_HIDDEN_VALUE = false
        const val NO_IP = "-1.-1.-1.-1"
        const val NO_TIMESTAMP: Long = Long.MIN_VALUE
        const val NO_PATH = ""
        const val NO_RESPONSE = -1
        const val NO_REFERRER = "-"
        const val NO_AGENT = "-"
        const val NO_HASH = ""
    }

    private fun purePathGen(): String {
        val queryStart = requestPath.indexOf('?')
        val path = (if (queryStart < 0) requestPath else requestPath.substring(0, queryStart)).trim()
        return if (path.isEmpty() || path[path.length - 1] != '/') "$path/" else path
    }
}