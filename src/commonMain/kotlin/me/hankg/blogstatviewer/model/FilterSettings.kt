package me.hankg.blogstatviewer.model

import kotlinx.serialization.Serializable
import me.hankg.blogstatviewer.filtering.*

@Serializable
data class BotSettings(
    val subString: String = "",
    val forReferrer: Boolean = false,
    val forAgent: Boolean = true,
)

@Serializable
data class FilterSettings(
    val knownBots: List<BotSettings> = listOf(),
    var knownBlockedIPs: List<String> = listOf(),
    val desiredRequestPathBases: List<String> = listOf(),
    val desiredResponses: List<Int> = listOf(),
    val earliestBlogYear: Int = 0,
    val latestBlogYear: Int = 0
)

fun FilterSettings.toFilterCriteriaList(): List<Filter<AccessLogEntry>> {
    val result = mutableListOf<Filter<AccessLogEntry>>()

    if (desiredResponses.isNotEmpty()) {
        result.add(AllowedResponseCodeFilter(desiredResponses))
    }

    if (earliestBlogYear > 0 && latestBlogYear > 0) {
        result.add(ArticleYearPathFilter(earliestBlogYear, latestBlogYear))
    }

    if (desiredRequestPathBases.isNotEmpty()) {
        result.add(RelPathStartFilter(desiredRequestPathBases))
    }

    if (knownBots.isNotEmpty()) {
        result.add(BotFilter(knownBots))
    }

    if (knownBlockedIPs.isNotEmpty()) {
        result.add(FilterBadIPs(knownBlockedIPs))
    }

    return result
}
