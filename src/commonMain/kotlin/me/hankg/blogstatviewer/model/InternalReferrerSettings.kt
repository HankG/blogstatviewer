package me.hankg.blogstatviewer.model

import kotlinx.serialization.Serializable

@Serializable
data class InternalReferrerSettings(
    val blogBaseUrl: String = "",
    val categories: Map<String, String> = mapOf()
)