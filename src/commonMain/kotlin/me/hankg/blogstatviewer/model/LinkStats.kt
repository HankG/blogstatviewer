package me.hankg.blogstatviewer.model

import kotlinx.datetime.LocalDate

enum class TimeBinSize {
    Day,
    Month,
    Year,
}

data class LinkStatsDateBin(
    val date: LocalDate,
    val stats: Map<String, LinkStats>
)

data class LinkStatSummary(
    val linkViews: Map<String, Int> = mutableMapOf(),
    val ipStats: MutableMap<String, Int> = mutableMapOf(),
    var viewCount: Int = 0,
    val externalReferrers: MutableMap<String, Int> = mutableMapOf(),
    val internalReferrers: MutableMap<String, Int> = mutableMapOf(),
    val browserNameStats: MutableMap<String, Int> = mutableMapOf(),
    val osNameStats: MutableMap<String, Int> = mutableMapOf()
) {
    companion object {
        fun summarize(bin: LinkStatsDateBin): LinkStatSummary {
            val viewCount = bin.stats.values.sumBy { it.viewCount }
            val ipAddresses = mutableMapOf<String, Int>()
            val referrers = mutableMapOf<String, Int>()
            val internalReferrers = mutableMapOf<String, Int>()
            val links = mutableMapOf<String, Int>()
            val osNamesStats = mutableMapOf<String, Int>()
            val browserNameStats = mutableMapOf<String, Int>()
            bin.stats.forEach { (key, value) ->
                links[key] = value.viewCount
                with(value.ipStats) {
                    this.forEach { (ip, count) ->
                        val currentCount = ipAddresses.getOrElse(ip) { 0 }
                        ipAddresses[ip] = currentCount + count
                    }
                }

                with(value.externalReferrers) {
                    this.forEach { (referrer, count) ->
                        val currentCount = referrers.getOrElse(referrer) { 0 }
                        referrers[referrer] = currentCount + count
                    }
                }

                with(value.internalReferrers) {
                    this.forEach { (referrer, count) ->
                        val currentCount = internalReferrers.getOrElse(referrer) { 0 }
                        internalReferrers[referrer] = currentCount + count
                    }
                }

                with(value.platformData) {
                    this.forEach { (platform, count) ->
                        val osCount = osNamesStats.getOrElse(platform.operatingSystem.name) { 0 }
                        val browserCount = browserNameStats.getOrElse(platform.browser.name) { 0 }
                        osNamesStats[platform.operatingSystem.name] = osCount + count
                        browserNameStats[platform.browser.name] = browserCount + count
                    }
                }
            }
            return LinkStatSummary(
                linkViews = links,
                viewCount = viewCount,
                ipStats = ipAddresses,
                externalReferrers = referrers,
                internalReferrers = internalReferrers,
                osNameStats = osNamesStats,
                browserNameStats = browserNameStats,
            )
        }

    }
}

data class LinkStats(
    val link: String = AccessLogEntry.NO_PATH,
    val ipStats: MutableMap<String, Int> = mutableMapOf(),
    var viewCount: Int = 0,
    val externalReferrers: MutableMap<String, Int> = mutableMapOf(),
    val internalReferrers: MutableMap<String, Int> = mutableMapOf(),
    val platformData: MutableMap<PlatformData, Int> = mutableMapOf()
)

data class PlatformData(
    val browser: BrowserData = BrowserData(),
    val operatingSystem: OperatingSystemData = OperatingSystemData()
)

data class BrowserData(
    val name: String = "",
    val version: String = ""
)

data class OperatingSystemData(
    val name: String = "",
    val version: String = "",
)