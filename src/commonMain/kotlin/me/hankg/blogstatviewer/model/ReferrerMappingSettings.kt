package me.hankg.blogstatviewer.model

import kotlinx.serialization.Serializable

@Serializable
data class ReferrerMappingSettings(
    val knownReferrerQueryStrings: Map<String, List<String>> = mapOf(),
    val knownReferrerSubstrings: Map<String, List<String>> = mapOf(),
)