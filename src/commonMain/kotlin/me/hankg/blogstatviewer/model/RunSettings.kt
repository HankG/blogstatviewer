package me.hankg.blogstatviewer.model

import kotlinx.serialization.Serializable

@Serializable
data class RunSettings(
    val logFolderPath: String = "",
    val dbPath: String = "",
    val summaryOutput: String = "",
    val filterSettings: String = "",
    val referrerMappingSettings: String = "",
    val internalMappingSettings: String = "",
    val badIpStudySettings: BadIpStudySettings = BadIpStudySettings(),
)

@Serializable
data class BadIpStudySettings(
    val windowSizeInMillis: Long = 10_000L,
    val maxPerWindow: Int = 20,
)