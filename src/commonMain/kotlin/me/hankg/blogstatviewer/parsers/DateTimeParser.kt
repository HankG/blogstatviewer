package me.hankg.blogstatviewer.parsers

expect object DateTimeParser {
    /**
     * Given a date string and the format of that string it converts it into the number of
     * milliseconds since 1/1/1970 00:00:00 as local date/time. All time zone information
     * will be ignored so local date/time strings should be passed in and used.
     */
    fun toUnixTime(dateString: String, format: String): Long

    /**
     * Given the Unix timestamp in milliseconds generate an ISO Date/Time string. It is
     * assuming all local date/time calculations therefore there should be no adjustment
     * for timezone changes.
     */
    fun toISOString(unixTimeMillis: Long): String
}