package me.hankg.blogstatviewer.parsers

import me.hankg.blogstatviewer.model.AccessLogEntry
import me.hankg.blogstatviewer.model.InternalReferrerSettings

class InternalReferrerParser(settings: InternalReferrerSettings) {
    companion object {
        const val BASE_LABEL = "Base"
        const val NOT_INTERNAL_REFERRAL = ""
        const val POST_CATEGORY = "Post"
        const val UNKNOWN_CATEGORY = "Unknown"
    }

    private val baseUrl: String
    private val categories: Map<String, String>

    init {
        baseUrl = getGoodUrl(settings.blogBaseUrl)
        categories = settings.categories
    }

    fun convertToType(entry: AccessLogEntry): String {
        val referrerUrl = entry.referrer

        if (baseUrl.startsWith("http") && (referrerUrl == baseUrl || referrerUrl == "$baseUrl/")) {
            return BASE_LABEL
        }

        if (referrerUrl == "https://$baseUrl"
            || referrerUrl == "https://www.$baseUrl"
            || referrerUrl == "https://$baseUrl/"
            || referrerUrl == "https://www.$baseUrl/"
        ) {
            return BASE_LABEL
        }

        if (!referrerUrl.contains(baseUrl)) {
            return NOT_INTERNAL_REFERRAL
        }

        val baseUrlStart = referrerUrl.indexOf(baseUrl)
        val firstPathPiece = referrerUrl.substring(baseUrlStart + baseUrl.length)
            .split("/")
            .firstOrNull { it.isNotEmpty() } ?: ""

        if (firstPathPiece.toIntOrNull() != null) {
            return POST_CATEGORY
        }

        categories.forEach { (category, subString) ->
            if (firstPathPiece.equals(subString, ignoreCase = true)) {
                return category
            }
        }

        return UNKNOWN_CATEGORY
    }

    private fun getGoodUrl(url: String): String {
        val trimmedUrl = url.trim()
        return if (trimmedUrl.endsWith("/"))
            trimmedUrl
        else
            "$trimmedUrl/"
    }
}