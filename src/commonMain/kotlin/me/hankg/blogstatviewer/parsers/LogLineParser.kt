package me.hankg.blogstatviewer.parsers

interface LogLineParser<T> {

    fun parse(line: String): T

}