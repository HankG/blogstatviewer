package me.hankg.blogstatviewer.parsers

import me.hankg.blogstatviewer.model.AccessLogEntry
import me.hankg.blogstatviewer.hashing.StringHashGenerator

class NginxDefaultAccessLineParser : LogLineParser<AccessLogEntry> {

    companion object {
        const val EXPECTED_REQUEST_TYPE = "GET"
        const val NGINX_DEFAULT_DATETIME_FORMAT = "d/MMM/yyyy:HH:mm:ss"
    }

    override fun parse(line: String): AccessLogEntry {
        if (line.isEmpty()) throw IllegalArgumentException("Need a non-empty string")
        val hash = StringHashGenerator.hashString(line)

        val ipSeperator = line.indexOf('-')
        val ipString = line.substring(0, ipSeperator).trim()

        val dateSepStart = line.indexOf('[', ipSeperator)
        val dateSepStop = line.indexOf('+', dateSepStart)
        val dateString = line.substring(dateSepStart + 1, dateSepStop).trim()
        val accessTime = DateTimeParser.toUnixTime(dateString, NGINX_DEFAULT_DATETIME_FORMAT)

        val requestStart = line.indexOf('\"')
        val requestStop = line.indexOf('\"', requestStart + 1)
        val requestString = line.substring(requestStart + 1, requestStop)
        val requestPieces = requestString.split("\\s+".toRegex())
        val path = if (requestPieces.size >= 2 && requestPieces[0] == EXPECTED_REQUEST_TYPE)
            requestPieces[1]
        else
            AccessLogEntry.NO_PATH

        val refererStart = line.indexOf('\"', requestStop + 1)
        val resultString = line.substring(requestStop + 1, refererStart).trim()
        val resultPieces = resultString.split("\\s+".toRegex())
        val (responseCode, returnedBytes) = if (resultPieces.size != 2) {
            Pair(AccessLogEntry.NO_RESPONSE, 0L)
        } else {
            Pair(resultPieces[0].toIntOrNull() ?: AccessLogEntry.NO_RESPONSE, resultPieces[1].toLongOrNull() ?: 0L)
        }

        val referrerStop = line.indexOf('\"', refererStart + 1)
        val referrer = line.substring(refererStart + 1, referrerStop).trim()

        val agentStart = line.indexOf('\"', referrerStop + 1)
        val agentStop = line.indexOf('\"', agentStart + 1)
        val agent = line.substring(agentStart + 1, agentStop).trim()

        return AccessLogEntry(
            ipAddress = if (ipString.isEmpty()) AccessLogEntry.NO_IP else ipString,
            accessTimeMillis = accessTime,
            requestPath = path,
            responseCode = responseCode,
            bytesSent = returnedBytes,
            referrer = if (referrer.isEmpty()) AccessLogEntry.NO_REFERRER else referrer,
            userAgent = if (agent.isEmpty()) AccessLogEntry.NO_AGENT else agent,
            textHash = hash
        )
    }
}