package me.hankg.blogstatviewer.parsers

import me.hankg.blogstatviewer.model.AccessLogEntry
import me.hankg.blogstatviewer.model.BrowserData
import me.hankg.blogstatviewer.model.OperatingSystemData
import me.hankg.blogstatviewer.model.PlatformData

class PlatformParser {
    fun getPlatform(entry: AccessLogEntry): PlatformData {
        val dataString = extractDataString(entry.userAgent)
        if (dataString.isEmpty()) {
            return PlatformData()
        }

        val sysInfoEnd = dataString.indexOf(')')
        if (sysInfoEnd < 0) {
            return PlatformData()
        }

        val sysInfoString = dataString.substring(1, sysInfoEnd)
        val otherData = dataString.substring(sysInfoEnd + 1)
        val browser = extractBrowser(otherData)
        val systemData = extractSystemData(sysInfoString)
        return PlatformData(browser, systemData)
    }

    private fun extractDataString(fullString: String): String {
        val agentStringLead = "Mozilla/5.0"
        val dataStart = fullString.indexOf(agentStringLead)
        return if (dataStart < 0)
            return ""
        else
            fullString.substring(dataStart + agentStringLead.length).trim()
    }

    private fun extractBrowser(data: String): BrowserData {
        val safariValue = "Safari"
        val chromeValue = "Chrome"
        val fields = data.split("\\s+".toRegex())
        if (fields.size < 2) return BrowserData(name = fields.getOrElse(0) { "" })

        val nextToLastElement = fields[fields.size - 2]
        val lastElement = fields.last()
        val browserElement = if (lastElement.contains(safariValue) && nextToLastElement.contains(chromeValue))
            nextToLastElement
        else
            lastElement

        val browserData = browserElement.split("/")
        val browserName = browserData.getOrElse(0) { "" }
        val browserVersion = browserData.getOrElse(1) { "" }
        return BrowserData(browserName, browserVersion)
    }

    private fun extractSystemData(data: String): OperatingSystemData {
        val fields = data.split(";").map { it.trim() }
        return extractiOS(fields)
            ?: extractAndroid(fields)
            ?: extractWindows(fields)
            ?: extractMac(fields)
            ?: extractLinuxOrUnix(fields)
            ?: OperatingSystemData(name = data)
    }

    private fun extractLinuxOrUnix(fields: List<String>): OperatingSystemData? {
        val linuxName = "Linux"
        val minOsFieldLength = 4
        val fieldValue = "X11"
        if (fieldValue != fields.firstOrNull()) return null
        val name = if (fields.firstOrNull { it.startsWith(linuxName) } != null)
            linuxName
        else
            fields.firstOrNull { it.length > minOsFieldLength }?.split("\\s+".toRegex())?.firstOrNull()
                ?: fieldValue
        return OperatingSystemData(name)
    }

    private fun extractMac(fields: List<String>): OperatingSystemData? {
        val name = "Macintosh"
        if (name != fields.firstOrNull()) return null
        val version = normalizeVersion(fields.getOrNull(1)?.split("\\s+".toRegex())?.last() ?: "")
        return OperatingSystemData(name, version)
    }

    private fun extractWindows(fields: List<String>): OperatingSystemData? {
        val name = "Windows"
        val fieldValue = "Windows NT"
        val windowsFields = fields.filter { it.contains(name) }
        if (windowsFields.isEmpty()) return null
        val version =
            normalizeVersion(
                windowsFields.firstOrNull { it.startsWith(fieldValue) }?.split("\\s+".toRegex())?.last()
                    ?: ""
            )
        return OperatingSystemData(name, version)
    }

    private fun extractAndroid(fields: List<String>): OperatingSystemData? {
        val androidLabel = "Android"
        val androidField = fields.firstOrNull { it.startsWith(androidLabel) } ?: return null
        val androidElements = androidField.split("\\s+".toRegex())
        val version = androidElements.getOrNull(1) ?: ""
        return OperatingSystemData(androidLabel, version)
    }

    private fun extractiOS(fields: List<String>): OperatingSystemData? {
        val osName = "iOS"
        val iPadName = "iPad"
        val iPhoneName = "iPhone"
        val osKeyword = "OS"
        if (fields.isEmpty()) return null
        if (!fields.first().contains(iPadName) && !fields.first().contains(iPhoneName)) return null

        val osFields = fields.getOrElse(1) { "" }.split("\\s+".toRegex()).filter { it.isNotEmpty() }
        val versionFieldIndex = osFields.indexOf(osKeyword) + 1
        val version = if (versionFieldIndex == 0) "" else normalizeVersion(osFields[versionFieldIndex])
        return OperatingSystemData(osName, version)
    }

    private fun normalizeVersion(versionString: String): String {
        return versionString.replace('_', '.')
    }
}