package me.hankg.blogstatviewer.parsers

import me.hankg.blogstatviewer.model.AccessLogEntry
import me.hankg.blogstatviewer.model.ReferrerMappingSettings

class ReferrerParser(private val mappings: ReferrerMappingSettings) {

    fun convertToType(entry: AccessLogEntry): ReferrerResult {
        val referrer = entry.referrer
        if (referrer == AccessLogEntry.NO_REFERRER) {
            mappings.knownReferrerQueryStrings.forEach { (type, codes) ->
                codes.forEach { code ->
                    if (entry.requestPath.contains(code, ignoreCase = true)) {
                        return ReferrerResult(type, true)
                    }
                }
            }
        }

        mappings.knownReferrerSubstrings.forEach { (type, subStrings) ->
            subStrings.forEach { subString ->
                if (referrer.contains(subString, ignoreCase = true)) {
                    return ReferrerResult(type, true)
                }
            }
        }

        return ReferrerResult(referrer, false)
    }
}

data class ReferrerResult(
    val calculatedValue: String,
    val isKnownMapping: Boolean,
)