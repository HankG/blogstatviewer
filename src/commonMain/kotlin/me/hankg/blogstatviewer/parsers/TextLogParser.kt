package me.hankg.blogstatviewer.parsers

import me.hankg.blogstatviewer.TextFileReader

class TextLogParser<T>(val lineParser: LogLineParser<T>) {
    fun process(pathToFile: String): List<T> {
        return TextFileReader.readAllLines(pathToFile)
            .map { lineParser.parse(it) }
            .toList()
    }
}