package me.hankg.blogstatviewer
import kotlin.test.Test
import kotlin.test.assertEquals

class TextFileReaderTest {

    @Test
    fun testReadFromResources() {
        val expectedLines = listOf("line1", "line2", "line3")
        val path = "src/commonTest/resources/test_file_reader.txt"
        val lines = TextFileReader.readAllLines(path)
        assertEquals(expectedLines, lines)
    }
}
