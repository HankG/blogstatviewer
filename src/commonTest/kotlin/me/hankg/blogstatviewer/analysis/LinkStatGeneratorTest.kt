package me.hankg.blogstatviewer.analysis

import me.hankg.blogstatviewer.model.*
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail
import kotlinx.datetime.*
import kotlin.test.assertTrue

class LinkStatGeneratorTest {
    private val externalReferrerSettings = ReferrerMappingSettings(
        knownReferrerSubstrings = mapOf(
            "Google" to listOf("google.com"),
            "Facebook" to listOf("facebook.com")
        )
    )

    private val internalReferrerSettings = InternalReferrerSettings(
        blogBaseUrl = "https://myblog.com",
        categories = mapOf(
            "Archive" to "archive"
        )
    )

    private val generator = LinkStatGenerator(externalReferrerSettings, internalReferrerSettings)

    @Test
    fun testWithEmptyDataSet() {
        val data = listOf<AccessLogEntry>()
        val expected = mapOf<String, LinkStats>()
        evaluate(data, expected)
    }

    @Test
    fun testWithSingleDataSet() {
        val ipAddress = "1.2.3.4"
        val accessTime = LocalDateTime(2021, 12, 15, 0, 0, 0)
            .toInstant(TimeZone.of("UTC")).toEpochMilliseconds()

        val path = "/2020/10/20/article/"
        val referrerString = "https://www.google.com/"
        val agent = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:85.0) Gecko/20100101 Firefox/85.0"
        val referrerType = "Google"
        val platformData = PlatformData(
            BrowserData("Firefox", "85.0"),
            OperatingSystemData("Linux")
        )
        val data = listOf(
            AccessLogEntry(
                ipAddress = ipAddress,
                accessTimeMillis = accessTime,
                requestPath = path,
                referrer = referrerString,
                userAgent = agent
            )
        )

        val expected = mapOf(
            path to LinkStats(
                link = path,
                ipStats = mutableMapOf(ipAddress to 1),
                viewCount = 1,
                externalReferrers = mutableMapOf(referrerType to 1),
                internalReferrers = mutableMapOf(),
                platformData = mutableMapOf(platformData to 1)
            )
        )
        evaluate(data, expected)
    }

    @Test
    fun testWithDiverseDataSet() {
        val ipAddress1 = "1.2.3.4"
        val ipAddress2 = "5.6.7.8"
        val accessTime = LocalDateTime(2021, 12, 15, 0, 0, 0)
            .toInstant(TimeZone.of("UTC")).toEpochMilliseconds()

        val path1 = "/2020/10/20/article/"
        val path2 = "/2019/03/17/article/"
        val referrerString1 = "https://www.google.com/"
        val referrerString2 = "https://m.facebook.com/"
        val referrerString3 = "${internalReferrerSettings.blogBaseUrl}/archive/2"
        val agent1 = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:85.0) Gecko/20100101 Firefox/85.0"
        val agent2 =
            "Mozilla/5.0 (iPad; CPU OS 10_3_3 like Mac OS X) AppleWebKit/603.3.8 (KHTML, like Gecko) Version/10.0 Mobile/14G60 Safari/602.1"
        val referrerType1 = "Google"
        val referrerType2 = "Facebook"
        val internalReferrer = "Archive"
        val platformData1 = PlatformData(
            BrowserData("Firefox", "85.0"),
            OperatingSystemData("Linux")
        )
        val platformData2 = PlatformData(
            BrowserData("Safari", "602.1"),
            OperatingSystemData("iOS", "10.3.3")
        )
        val data = listOf(
            AccessLogEntry(
                ipAddress = ipAddress1,
                accessTimeMillis = accessTime,
                requestPath = path1,
                referrer = referrerString1,
                userAgent = agent1
            ),
            AccessLogEntry(
                ipAddress = ipAddress2,
                accessTimeMillis = accessTime,
                requestPath = path1,
                referrer = referrerString3,
                userAgent = agent2
            ),
            AccessLogEntry(
                ipAddress = ipAddress2,
                accessTimeMillis = accessTime,
                requestPath = path2,
                referrer = referrerString2,
                userAgent = agent2
            ),
            AccessLogEntry(
                ipAddress = ipAddress2,
                accessTimeMillis = accessTime,
                requestPath = path2,
                referrer = referrerString2,
                userAgent = agent2
            )
        )

        val expected = mapOf(
            path1 to LinkStats(
                link = path1,
                ipStats = mutableMapOf(ipAddress1 to 1, ipAddress2 to 1),
                viewCount = 2,
                externalReferrers = mutableMapOf(referrerType1 to 1),
                internalReferrers = mutableMapOf(internalReferrer to 1),
                platformData = mutableMapOf(platformData1 to 1, platformData2 to 1)
            ),
            path2 to LinkStats(
                link = path2,
                ipStats = mutableMapOf(ipAddress2 to 2),
                viewCount = 2,
                externalReferrers = mutableMapOf(referrerType2 to 2),
                internalReferrers = mutableMapOf(),
                platformData = mutableMapOf(platformData2 to 2)
            ),
        )
        evaluate(data, expected)
    }

    @Test
    fun testBinnedWithEmptyDataSet() {
        assertTrue(generator.generateByTimeBuckets(listOf(), TimeBinSize.Month).isEmpty())
    }

    @Test
    fun testBinnedWithDiverseDataSet() {
        val ipAddress1 = "1.2.3.4"
        val ipAddress2 = "5.6.7.8"
        val accessTime1 = LocalDateTime(2020, 11, 14, 0, 0, 0)
            .toInstant(TimeZone.of("UTC")).toEpochMilliseconds()
        val accessTime2 = LocalDateTime(2021, 12, 15, 0, 0, 0)
            .toInstant(TimeZone.of("UTC")).toEpochMilliseconds()

        val path1 = "/2020/10/20/article/"
        val path2 = "/2019/03/17/article/"
        val referrerString1 = "https://www.google.com/"
        val referrerString2 = "https://m.facebook.com/"
        val referrerString3 = "${internalReferrerSettings.blogBaseUrl}/archive/2"
        val agent1 = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:85.0) Gecko/20100101 Firefox/85.0"
        val agent2 =
            "Mozilla/5.0 (iPad; CPU OS 10_3_3 like Mac OS X) AppleWebKit/603.3.8 (KHTML, like Gecko) Version/10.0 Mobile/14G60 Safari/602.1"
        val referrerType1 = "Google"
        val referrerType2 = "Facebook"
        val internalReferrer = "Archive"
        val platformData1 = PlatformData(
            BrowserData("Firefox", "85.0"),
            OperatingSystemData("Linux")
        )
        val platformData2 = PlatformData(
            BrowserData("Safari", "602.1"),
            OperatingSystemData("iOS", "10.3.3")
        )
        val data = listOf(
            AccessLogEntry(
                ipAddress = ipAddress1,
                accessTimeMillis = accessTime1,
                requestPath = path1,
                referrer = referrerString1,
                userAgent = agent1
            ),
            AccessLogEntry(
                ipAddress = ipAddress2,
                accessTimeMillis = accessTime2,
                requestPath = path1,
                referrer = referrerString3,
                userAgent = agent2
            ),
            AccessLogEntry(
                ipAddress = ipAddress2,
                accessTimeMillis = accessTime1,
                requestPath = path2,
                referrer = referrerString2,
                userAgent = agent2
            ),
            AccessLogEntry(
                ipAddress = ipAddress2,
                accessTimeMillis = accessTime2,
                requestPath = path2,
                referrer = referrerString2,
                userAgent = agent2
            )
        )

        val expected = listOf(
            LinkStatsDateBin(
                LocalDate(2020, 11, 1),
                mapOf(
                    path1 to LinkStats(
                        link = path1,
                        ipStats = mutableMapOf(ipAddress1 to 1),
                        viewCount = 1,
                        externalReferrers = mutableMapOf(referrerType1 to 1),
                        internalReferrers = mutableMapOf(),
                        platformData = mutableMapOf(platformData1 to 1)
                    ),
                    path2 to LinkStats(
                        link = path2,
                        ipStats = mutableMapOf(ipAddress2 to 1),
                        viewCount = 1,
                        externalReferrers = mutableMapOf(referrerType2 to 1),
                        internalReferrers = mutableMapOf(),
                        platformData = mutableMapOf(platformData2 to 1)
                    ),
                ),
            ),
            LinkStatsDateBin(
                LocalDate(2021, 12, 1),
                mapOf(
                    path1 to LinkStats(
                        link = path1,
                        ipStats = mutableMapOf(ipAddress2 to 1),
                        viewCount = 1,
                        externalReferrers = mutableMapOf(),
                        internalReferrers = mutableMapOf(internalReferrer to 1),
                        platformData = mutableMapOf(platformData2 to 1)
                    ),
                    path2 to LinkStats(
                        link = path2,
                        ipStats = mutableMapOf(ipAddress2 to 1),
                        viewCount = 1,
                        externalReferrers = mutableMapOf(referrerType2 to 1),
                        internalReferrers = mutableMapOf(),
                        platformData = mutableMapOf(platformData2 to 1)
                    ),
                ),
            ),
        )

        assertEquals(expected, generator.generateByTimeBuckets(data, TimeBinSize.Month))
    }

    private fun evaluate(data: List<AccessLogEntry>, expected: Map<String, LinkStats>) {
        val actual = generator.generate(data)
        assertEquals(expected, actual)
    }
}