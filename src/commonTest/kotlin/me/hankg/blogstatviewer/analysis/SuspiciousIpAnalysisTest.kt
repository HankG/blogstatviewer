package me.hankg.blogstatviewer.analysis

import me.hankg.blogstatviewer.model.AccessLogEntry
import kotlin.test.Test
import kotlin.test.assertEquals

class SuspiciousIpAnalysisTest {

    @Test
    fun testWithEmptyDataAndDefaults() {
        assertEquals(listOf(), SuspiciousIpAnalysis().findIps(listOf()))
    }

    @Test
    fun testFindIps() {
        val windowInMillis = 1000L
        val maxInWindow = 2
        val entries = listOf(
            AccessLogEntry(ipAddress = "1", accessTimeMillis = 1000),
            AccessLogEntry(ipAddress = "1", accessTimeMillis = 1000),
            AccessLogEntry(ipAddress = "1", accessTimeMillis = 1000),
            AccessLogEntry(ipAddress = "1", accessTimeMillis = 2000),
            AccessLogEntry(ipAddress = "2", accessTimeMillis = 1000),
            AccessLogEntry(ipAddress = "2", accessTimeMillis = 2000),
            AccessLogEntry(ipAddress = "2", accessTimeMillis = 3000),
            AccessLogEntry(ipAddress = "2", accessTimeMillis = 4000),
            AccessLogEntry(ipAddress = "3", accessTimeMillis = 1000),
            AccessLogEntry(ipAddress = "3", accessTimeMillis = 1000),
            AccessLogEntry(ipAddress = "3", accessTimeMillis = 5000),
            AccessLogEntry(ipAddress = "3", accessTimeMillis = 6000),
            AccessLogEntry(ipAddress = "4", accessTimeMillis = 1000),
            AccessLogEntry(ipAddress = "4", accessTimeMillis = 1000),
            AccessLogEntry(ipAddress = "4", accessTimeMillis = 2000),
            AccessLogEntry(ipAddress = "4", accessTimeMillis = 2000),
            AccessLogEntry(ipAddress = "5", accessTimeMillis = 1000),
            AccessLogEntry(ipAddress = "5", accessTimeMillis = 1001),
            AccessLogEntry(ipAddress = "5", accessTimeMillis = 1002),
        )

        val expected = listOf("1", "5")

        assertEquals(expected, SuspiciousIpAnalysis(windowInMillis, maxInWindow).findIps(entries))
    }
}