package me.hankg.blogstatviewer.filtering

import me.hankg.blogstatviewer.model.AccessLogEntry
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class AllowedResponseCodeFilterTest {
    val entries = listOf(200, 404, 301, 500, 200, 201).map { AccessLogEntry(responseCode = it) }

    @Test
    fun testResponseCodeFiltering() {
        val filter = AllowedResponseCodeFilter(listOf(200, 201))
        val expected = listOf(true, false, false, false, true, true)
        val actual = entries.map { filter.checkIsOkay(it) }
        assertEquals(expected, actual)
    }

    @Test
    fun testWithEmptyList() {
        val filter = AllowedResponseCodeFilter()
        entries.map { filter.checkIsOkay(it) }.forEach { assertTrue(it) }
    }
}