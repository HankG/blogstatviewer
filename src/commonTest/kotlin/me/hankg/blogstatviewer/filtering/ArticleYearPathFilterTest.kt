package me.hankg.blogstatviewer.filtering

import me.hankg.blogstatviewer.model.AccessLogEntry
import kotlin.test.Test
import kotlin.test.assertEquals

class ArticleYearPathFilterTest {

    @Test
    fun testYearFilter() {
        val filter = ArticleYearPathFilter(2006, 2009)
        val entries = listOf(
            AccessLogEntry(requestPath = "/2005/10/17/article"),
            AccessLogEntry(requestPath = "/2006/10/17/article"),
            AccessLogEntry(requestPath = "/2007/10/17/article"),
            AccessLogEntry(requestPath = "/2008/10/17/article"),
            AccessLogEntry(requestPath = "/2009/10/17/article"),
            AccessLogEntry(requestPath = "/2010/10/17/article"),
            AccessLogEntry(requestPath = "/pages/1"),
            AccessLogEntry(requestPath = "/"),
            AccessLogEntry(requestPath = "something else"),
        )
        val expected = listOf(false, true, true, true, true, false, false, false, false)
        val actual = entries.map { filter.checkIsOkay(it) }
        assertEquals(expected, actual)
    }
}