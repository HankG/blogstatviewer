package me.hankg.blogstatviewer.filtering

import me.hankg.blogstatviewer.model.AccessLogEntry
import me.hankg.blogstatviewer.model.BotSettings
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class BotFilterTest {
    val botNameSubstr = listOf("bingbot", "bot1", "badactor", "noiz.com", "Feed Wrangler")
        .map { BotSettings(it, true, true) }

    @Test
    fun testOnlyReferrer() {
        val filter = BotFilter(listOf(BotSettings("bot", true, false)))
        assertFalse(filter.checkIsOkay(AccessLogEntry(referrer = "bot")))
        assertTrue(filter.checkIsOkay(AccessLogEntry(userAgent = "bot")))
    }

    @Test
    fun testOnlyAgent() {
        val filter = BotFilter(listOf(BotSettings("bot", false, true)))
        assertTrue(filter.checkIsOkay(AccessLogEntry(referrer = "bot")))
        assertFalse(filter.checkIsOkay(AccessLogEntry(userAgent = "bot")))
    }

    @Test
    fun testFilteringFromReferrer() {
        val filter = BotFilter(botNameSubstr)
        val entries = listOf(
            AccessLogEntry(referrer = "https://google.com"),
            AccessLogEntry(referrer = "-"),
            AccessLogEntry(referrer = ""),
            AccessLogEntry(referrer = "https://s1.d1.bot1.com"),
            AccessLogEntry(referrer = "https://badactor1.com"),
            AccessLogEntry(referrer = "noiz.com"),
        )
        val expected = listOf(true, true, true, false, false, false)
        val actual = entries.map { filter.checkIsOkay(it) }
        assertEquals(expected, actual)
    }

    @Test
    fun testFilteringFromAgent() {
        val filter = BotFilter(botNameSubstr)
        val entries = listOf(
            AccessLogEntry(userAgent = "Feed Wrangler/1.0 (1 subscriber; feed-id=479242; http://feedwrangler.net; Allow like Gecko)"),
            AccessLogEntry(userAgent = "-"),
            AccessLogEntry(userAgent = ""),
            AccessLogEntry(userAgent = "Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm"),
            AccessLogEntry(userAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:85.0) Gecko/20100101 Firefox/85.0"),
            AccessLogEntry(userAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36"),
        )
        val expected = listOf(false, true, true, false, true, true)
        val actual = entries.map { filter.checkIsOkay(it) }
        assertEquals(expected, actual)
    }

    @Test
    fun testWithEmptyList() {
        val filter = BotFilter()
        val entries = listOf(
            AccessLogEntry(
                referrer = "https://s1.d1.bot1.com",
                userAgent = "Feed Wrangler/1.0 (1 subscriber; feed-id=479242; http://feedwrangler.net; Allow like Gecko)"
            ),
            AccessLogEntry(referrer = "", userAgent = ""),
            AccessLogEntry(
                referrer = "https://s1.d1.bot1.com",
                userAgent = "Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm"
            ),
            AccessLogEntry(
                referrer = "https://badactor1.com",
                userAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:85.0) Gecko/20100101 Firefox/85.0"
            ),
            AccessLogEntry(
                referrer = "noiz.com",
                userAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36"
            ),
        )
        val expected = listOf(true, true, true, true, true)
        val actual = entries.map { filter.checkIsOkay(it) }
        assertEquals(expected, actual)
    }
}