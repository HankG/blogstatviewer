package me.hankg.blogstatviewer.filtering

import me.hankg.blogstatviewer.model.AccessLogEntry
import kotlin.test.Test
import kotlin.test.assertEquals

class FilterBadIPsTest {

    @Test
    fun testFilteringBadIps() {
        val badIPs = listOf("1.1.1.1", "4.4.4.4")
        val filter = FilterBadIPs(badIPs)
        val ipsToTest = (1..5).map { AccessLogEntry(ipAddress = "$it.$it.$it.$it") }
        val expected = listOf(false, true, true, false, true)
        val actual = ipsToTest.map { filter.checkIsOkay(it) }
        assertEquals(expected, actual)
    }

    @Test
    fun testWithEmptyList() {
        val filter = FilterBadIPs()
        val ipsToTest = listOf("1.2.3.4", "2085::50", "something else").map { AccessLogEntry(ipAddress = it) }
        val expected = listOf(true, true, true)
        val actual = ipsToTest.map { filter.checkIsOkay(it) }
        assertEquals(expected, actual)

    }
}