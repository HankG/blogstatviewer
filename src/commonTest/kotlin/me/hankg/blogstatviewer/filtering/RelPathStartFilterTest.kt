package me.hankg.blogstatviewer.filtering

import me.hankg.blogstatviewer.model.AccessLogEntry
import kotlin.test.Test
import kotlin.test.assertEquals

class RelPathStartFilterTest {

    @Test
    fun testFilterRelativePath() {
        val pathStartList = listOf(
            "/2020",
            "/hello",
            "/feed.xml",
        )
        val filter = RelPathStartFilter(pathStartList)

        val entry = listOf(
            AccessLogEntry(requestPath = ""),
            AccessLogEntry(requestPath = "/"),
            AccessLogEntry(requestPath = "/hello"),
            AccessLogEntry(requestPath = "/2020/hello"),
            AccessLogEntry(requestPath = "/2020/again"),
            AccessLogEntry(requestPath = "/wp-content/feed.xml"),
            AccessLogEntry(requestPath = "/feed.xml"),
        )
        val expected = listOf(false, false, true, true, true, false, true)
        val actual = entry.map { filter.checkIsOkay(it) }
        assertEquals(expected, actual)
    }

    @Test
    fun testFilterWithEmptyList() {
        val entry = listOf(
            AccessLogEntry(requestPath = ""),
            AccessLogEntry(requestPath = "/"),
            AccessLogEntry(requestPath = "/hello")
        )
        val filter = RelPathStartFilter()
        val expected = listOf(true, true, true)
        val actual = entry.map { filter.checkIsOkay(it) }
        assertEquals(expected, actual)
    }

}