package me.hankg.blogstatviewer.model

import me.hankg.blogstatviewer.filtering.BotFilter
import me.hankg.blogstatviewer.filtering.FilterBadIPs
import me.hankg.blogstatviewer.filtering.checkIsOkay
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class AccessLogEntryFilterTest {


    @Test
    fun testWithEmptyItemAndCriteria() {
        assertTrue(AccessLogEntry().checkIsOkay(listOf()))
    }

    @Test
    fun testWithEmptyCriteria() {
        assertTrue(
            AccessLogEntry(
                ipAddress = "1.2.3.4",
                accessTimeMillis = 10,
                requestPath = "/",
                responseCode = 200,
                referrer = "https://google.com",
                userAgent = "Mozilla",
            ).checkIsOkay(listOf())
        )
    }

    @Test
    fun testWithFullCriteria() {
        val badIP = "1.2.3.4"
        val goodIP = "5.6.7.8"
        val botName = "bot.com"
        val badReferrer = "http://bot.com"
        val goodReferrer = "http://google.com"
        val criteria = listOf(
            BotFilter(listOf(botName, "anotherbot").map { BotSettings(it, true, true) }),
            FilterBadIPs(listOf(badIP))
        )

        val entries = listOf(
            AccessLogEntry(ipAddress = goodIP, referrer = goodReferrer),
            AccessLogEntry(ipAddress = badIP, referrer = goodReferrer),
            AccessLogEntry(ipAddress = goodIP, referrer = badReferrer),
            AccessLogEntry(ipAddress = badIP, referrer = badReferrer),
        )

        val expected = listOf(true, false, false, false)
        val actual = entries.map { it.checkIsOkay(criteria) }
        assertEquals(expected, actual)
    }
}