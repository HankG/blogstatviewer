package me.hankg.blogstatviewer.model

import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals

class FilterSettingsTest {
    @Test
    fun testDefaultSerialization() {
        testSerialization(FilterSettings())
    }

    @Test
    fun testFullObjectSerialization() {
        val settings = FilterSettings(
            knownBots = listOf("bot1", "bot2").map { BotSettings(it, true, false) },
            knownBlockedIPs = listOf("1.1.1.1", "2.2.2.2", "2345:0425:2CA1::0567:5673:23b5"),
            desiredRequestPathBases = listOf("feed.xml", "tags"),
            desiredResponses = listOf(200, 301),
            earliestBlogYear = 2005,
            latestBlogYear = 2007
        )
        testSerialization(settings)
    }

    private fun testSerialization(settings: FilterSettings) {
        val settingsJson = Json { prettyPrint = true }.encodeToString(settings)
        println(settingsJson)
        val settingsFromString = Json.decodeFromString<FilterSettings>(settingsJson)
        assertEquals(settings, settingsFromString)
    }
}