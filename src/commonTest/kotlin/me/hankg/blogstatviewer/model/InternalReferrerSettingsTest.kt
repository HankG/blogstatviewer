package me.hankg.blogstatviewer.model

import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals

class InternalReferrerSettingsTest {
    @Test
    fun testDefaultSerialization() {
        testSerialization(InternalReferrerSettings())
    }

    @Test
    fun testFullObjectSerialization() {
        val settings = InternalReferrerSettings(
            blogBaseUrl = "https://myblog.com",
            categories = mapOf("Archive" to "archive", "Tags" to "tag")
        )
        testSerialization(settings)
    }

    private fun testSerialization(settings: InternalReferrerSettings) {
        val settingsJson = Json { prettyPrint = true }.encodeToString(settings)
        println(settingsJson)
        val settingsFromString = Json.decodeFromString<InternalReferrerSettings>(settingsJson)
        assertEquals(settings, settingsFromString)
    }
}