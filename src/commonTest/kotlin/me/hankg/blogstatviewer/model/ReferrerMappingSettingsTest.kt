package me.hankg.blogstatviewer.model

import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals

class ReferrerMappingSettingsTest {
    @Test
    fun testDefaultSerialization() {
        testSerialization(ReferrerMappingSettings())
    }

    @Test
    fun testFullObjectSerialization() {
        testSerialization(
            ReferrerMappingSettings(
                knownReferrerQueryStrings = mapOf(
                    "Facebook" to listOf("fbclid"),
                    "Google" to listOf("gclid")
                ),
                knownReferrerSubstrings = mapOf(
                    "Facebook" to listOf("facebook.com"),
                    "Google" to listOf("www.google"),
                    "Twitter" to listOf("t.co")
                ),
            )
        )
    }

    private fun testSerialization(settings: ReferrerMappingSettings) {
        val settingsJson = Json { prettyPrint = true }.encodeToString(settings)
        println(settingsJson)
        val settingsFromString = Json.decodeFromString<ReferrerMappingSettings>(settingsJson)
        assertEquals(settings, settingsFromString)
    }
}