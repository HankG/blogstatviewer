package me.hankg.blogstatviewer.model

import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals

class RunSettingsTest {
    @Test
    fun testDefaultSerialization() {
        testSerialization(RunSettings())
    }

    @Test
    fun testFullObjectSerialization() {
        testSerialization(
            RunSettings(
                logFolderPath = "/var/log/nginx",
                dbPath = "/home/user1/blogdata.db",
                summaryOutput = "/tmp/output.txt",
                filterSettings = "/tmp/filterConfig.json",
                referrerMappingSettings = "/tmp/externalReferrerConfig.json",
                internalMappingSettings = "/tmp/internalReferrerConfig.json",
                badIpStudySettings = BadIpStudySettings(1000L, 2)
            )
        )
    }

    private fun testSerialization(settings: RunSettings) {
        val settingsJson = Json { prettyPrint = true }.encodeToString(settings)
        println(settingsJson)
        val settingsFromString = Json.decodeFromString<RunSettings>(settingsJson)
        assertEquals(settings, settingsFromString)
    }
}