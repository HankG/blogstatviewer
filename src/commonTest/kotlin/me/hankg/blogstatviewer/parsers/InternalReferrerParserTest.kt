package me.hankg.blogstatviewer.parsers

import me.hankg.blogstatviewer.model.AccessLogEntry
import me.hankg.blogstatviewer.model.InternalReferrerSettings
import kotlin.test.Test
import kotlin.test.assertEquals

class InternalReferrerParserTest {
    val settings = InternalReferrerSettings(
        blogBaseUrl = "https://myblog.com/",
        categories = mapOf("Archive" to "archive", "Tags" to "tags")
    )
    val parser = InternalReferrerParser(settings)

    @Test
    fun testNoReferrer() {
        assertEquals(InternalReferrerParser.NOT_INTERNAL_REFERRAL, parser.convertToType(AccessLogEntry()))
    }

    @Test
    fun testBaseReferrer() {
        val url = settings.blogBaseUrl
        assertEquals(InternalReferrerParser.BASE_LABEL, parser.convertToType(AccessLogEntry(referrer = url)))
    }

    @Test
    fun testKnownCategory() {
        val url = "${settings.blogBaseUrl}/tags/"
        assertEquals("Tags", parser.convertToType(AccessLogEntry(referrer = url)))
    }

    @Test
    fun testUnknownCategory() {
        val url = "${settings.blogBaseUrl}/somethingelse/"
        assertEquals(InternalReferrerParser.UNKNOWN_CATEGORY, parser.convertToType(AccessLogEntry(referrer = url)))
    }

    @Test
    fun testAnotherPostReferrer() {
        val url = "${settings.blogBaseUrl}/2020/10/20/article"
        assertEquals(InternalReferrerParser.POST_CATEGORY, parser.convertToType(AccessLogEntry(referrer = url)))
    }

    @Test
    fun testOutsideReferrer() {
        assertEquals(
            InternalReferrerParser.NOT_INTERNAL_REFERRAL,
            parser.convertToType(AccessLogEntry(referrer = "external link"))
        )
        assertEquals(
            InternalReferrerParser.NOT_INTERNAL_REFERRAL,
            parser.convertToType(AccessLogEntry(referrer = "https://google.com"))
        )
    }
}