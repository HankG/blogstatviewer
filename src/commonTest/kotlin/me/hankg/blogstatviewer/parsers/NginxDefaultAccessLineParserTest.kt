import me.hankg.blogstatviewer.model.AccessLogEntry
import me.hankg.blogstatviewer.parsers.NginxDefaultAccessLineParser
import java.lang.IllegalArgumentException
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

class NginxDefaultAccessLineParserTest {

    @Test
    fun testActualLogLines() {
        val line1 =
            "1.2.3.4 - - [25/Feb/2021:06:42:18 +0000] \"GET /robots.txt HTTP/1.1\" 301 194 \"-\" \"(compatible;PetalBot;+https://aspiegel.com/petalbot)\""
        val line1ExpectedEntry = AccessLogEntry(
            ipAddress = "1.2.3.4",
            accessTimeMillis = 1614235338000,
            requestPath = "/robots.txt",
            responseCode = 301,
            bytesSent = 194,
            referrer = "-",
            userAgent = "(compatible;PetalBot;+https://aspiegel.com/petalbot)",
            textHash = "8f51fe188d83616ed74c20925127447d9b78b5e38d3d84d0ecaa6ef74d0ddf69"
        )
        val line2 =
            "100.120.130.140 - - [25/Feb/2021:06:41:42 +0000] \"\\x03\\x00\\x00/*\\xE0\\x00\\x00\\x00\\x00\\x00Cookie: mstshash=Administr\" 400 182 \"-\" \"-\""
        val line2ExpectedEntry = AccessLogEntry(
            ipAddress = "100.120.130.140",
            accessTimeMillis = 1614235302000,
            requestPath = "",
            responseCode = 400,
            bytesSent = 182,
            referrer = "-",
            userAgent = "-",
            textHash = "436e6ede3c9cbe4076ea2aa65c3b261bacc2717834eb11be5e5990687f72e72c"
        )
        assertEquals(line1ExpectedEntry, NginxDefaultAccessLineParser().parse(line1))
        assertEquals(line2ExpectedEntry, NginxDefaultAccessLineParser().parse(line2))
    }

    @Test
    fun testEmptyLine() {
        try {
            println(NginxDefaultAccessLineParser().parse(""))
            fail()
        } catch (e: IllegalArgumentException) {

        }
    }

    @Test
    fun testLineWithfullIPV6() {
        val line =
            "2001:0000:3238:DFE1:0063:0000:0000:FEFB - - [25/Feb/2021:06:42:18 +0000] \"GET /robots.txt HTTP/1.1\" 301 194 \"-\" \"(compatible;PetalBot;+https://aspiegel.com/petalbot)\""
        val expectedIpAddress = "2001:0000:3238:DFE1:0063:0000:0000:FEFB"
        val entry = NginxDefaultAccessLineParser().parse(line)
        assertEquals(expectedIpAddress, entry.ipAddress)
    }

    @Test
    fun testMinimalLineWithBadRequestData() {
        val line = " - - [25/Feb/2021:06:42:18 +0000] \"\" \"\" \"\""
        val expectedEntry = AccessLogEntry(
            ipAddress = AccessLogEntry.NO_IP,
            accessTimeMillis = 1614235338000,
            requestPath = AccessLogEntry.NO_PATH,
            responseCode = AccessLogEntry.NO_RESPONSE,
            bytesSent = 0,
            referrer = AccessLogEntry.NO_REFERRER,
            userAgent = AccessLogEntry.NO_AGENT,
            textHash = "cf652cf065bb35784570dc138e32f70ab62904a3d3b7d9d38093407d535cf02a"
        )
        assertEquals(expectedEntry, NginxDefaultAccessLineParser().parse(line))
    }

    @Test
    fun testLineWithMinimalData() {
        val line = " - - [25/Feb/2021:06:42:18 +0000] \"\" 0 0 \"\" \"\""
        val expectedEntry = AccessLogEntry(
            ipAddress = AccessLogEntry.NO_IP,
            accessTimeMillis = 1614235338000,
            requestPath = AccessLogEntry.NO_PATH,
            responseCode = 0,
            bytesSent = 0,
            referrer = AccessLogEntry.NO_REFERRER,
            userAgent = AccessLogEntry.NO_AGENT,
            textHash = "901328cc5eb715e4c255be71d97ca8d25c9b8c4b74afc970050d4d9bf6af17f0"
        )
        assertEquals(expectedEntry, NginxDefaultAccessLineParser().parse(line))
    }

}