package me.hankg.blogstatviewer.parsers

import kotlin.test.Test
import kotlin.test.assertEquals

class TextLogParserTest {
    @Test
    fun testLogParserWithMock() {
        val expectedLines = listOf("Read line: line1", "Read line: line2", "Read line: line3")
        val path = "src/commonTest/resources/test_file_reader.txt"
        val lines = TextLogParser(MockLineParser()).process(path)
        assertEquals(expectedLines, lines)
    }
}

class MockLineParser : LogLineParser<String> {
    override fun parse(line: String): String {
        return "Read line: $line"
    }
}