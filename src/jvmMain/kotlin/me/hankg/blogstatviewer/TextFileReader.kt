package me.hankg.blogstatviewer

import java.nio.file.Files
import java.nio.file.Paths

actual object TextFileReader {
    actual fun readAllLines(filePath:String):Collection<String> {
        val path = Paths.get(filePath)
        return Files.readAllLines(path).toList()
    }
}