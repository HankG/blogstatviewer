package me.hankg.blogstatviewer.data

actual object AccessLogRepositoryFactory {

    actual fun getSqliteDbFromFilePath(filePath: String): AccessLogRepository {
        val connectionString = generateSqliteConnectionString(filePath)
        return getSqliteDatabase(connectionString)
    }

    actual fun getSqliteDatabase(connectionString: String): AccessLogRepository {
        return JvmSqlLiteAccessLogRepository(connectionString)
    }

    actual fun generateSqliteConnectionString(filePathtoDb: String): String {
        return "jdbc:sqlite:$filePathtoDb"
    }
}