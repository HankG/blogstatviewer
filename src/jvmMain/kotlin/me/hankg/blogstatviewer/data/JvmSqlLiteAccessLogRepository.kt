package me.hankg.blogstatviewer.data

import me.hankg.blogstatviewer.model.AccessLogEntry
import org.jetbrains.exposed.dao.id.UUIDTable
import org.jetbrains.exposed.exceptions.ExposedSQLException
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.statements.InsertStatement
import org.jetbrains.exposed.sql.transactions.transaction

class JvmSqlLiteAccessLogRepository(connectionString: String) : AccessLogRepository {
    private val db: Database = Database.connect(connectionString, DRIVER_TYPE)

    companion object {
        private const val DRIVER_TYPE = "org.sqlite.JDBC"
    }

    init {
        transaction(db) {
            SchemaUtils.create(DbAccessLogEntry)
        }
    }

    override fun countAll(): Long {
        return transaction(db) {
            DbAccessLogEntry.selectAll().count()
        }
    }

    override fun findAll(excludeHidden: Boolean): Collection<AccessLogEntry> {
        return transaction(db) {
            val query = if (excludeHidden) {
                DbAccessLogEntry.select { DbAccessLogEntry.hidden eq false }
            } else {
                DbAccessLogEntry.selectAll()
            }

            query.map { it.toAccessLogEntry() }
        }
    }

    override fun findByIp(ip: String): Collection<AccessLogEntry> {
        return transaction(db) {
            DbAccessLogEntry.select { DbAccessLogEntry.ipAddress eq ip }
                .map { it.toAccessLogEntry() }
        }
    }

    override fun insert(entry: AccessLogEntry): Boolean {
        return try {
            transaction(db) {
                entry.insert()
            }
            true
        } catch (e: ExposedSQLException) {
            false
        }
    }

    override fun insertMany(entries: Collection<AccessLogEntry>): Boolean {
        return try {
            transaction(db) {
                entries.batchInsert()
            }
            true
        } catch (e: ExposedSQLException) {
            false
        }
    }

    override fun update(entry: AccessLogEntry): Boolean {
        val count = transaction(db) {
            DbAccessLogEntry.update({ DbAccessLogEntry.hash eq entry.textHash }) {
                it[ipAddress] = entry.ipAddress
                it[accessTimeMillis] = entry.accessTimeMillis
                it[requestPath] = entry.requestPath
                it[responseCode] = entry.responseCode
                it[bytesSent] = entry.bytesSent
                it[referrer] = entry.referrer
                it[userAgent] = entry.userAgent
                it[hidden] = entry.hidden
            }
        }

        return count == 1
    }

}

private object DbAccessLogEntry : UUIDTable() {
    val ipAddress = text("IPAddress").index("IPIndex")
    val accessTimeMillis = long("AccessTime").index("AccessTimeIndex")
    val requestPath = text("RequestPath")
    val responseCode = integer("ResponseCode")
    val bytesSent = long("BytesSent")
    val referrer = text("Referrer")
    val userAgent = text("UserAgent")
    val hidden = bool("Hidden")
    val hash = text("Hash").uniqueIndex("HashIndex")
}

private fun AccessLogEntry.insert(): InsertStatement<Number> {
    val entry = this
    return DbAccessLogEntry.insert {
        it[ipAddress] = entry.ipAddress
        it[accessTimeMillis] = entry.accessTimeMillis
        it[requestPath] = entry.requestPath
        it[responseCode] = entry.responseCode
        it[bytesSent] = entry.bytesSent
        it[referrer] = entry.referrer
        it[userAgent] = entry.userAgent
        it[hidden] = entry.hidden
        it[hash] = entry.textHash
    }
}

private fun Collection<AccessLogEntry>.batchInsert(): List<ResultRow> {
    val entries = this
    return DbAccessLogEntry.batchInsert(data = entries, ignore = true) { entry: AccessLogEntry ->
        this[DbAccessLogEntry.ipAddress] = entry.ipAddress
        this[DbAccessLogEntry.accessTimeMillis] = entry.accessTimeMillis
        this[DbAccessLogEntry.requestPath] = entry.requestPath
        this[DbAccessLogEntry.responseCode] = entry.responseCode
        this[DbAccessLogEntry.bytesSent] = entry.bytesSent
        this[DbAccessLogEntry.referrer] = entry.referrer
        this[DbAccessLogEntry.userAgent] = entry.userAgent
        this[DbAccessLogEntry.hidden] = entry.hidden
        this[DbAccessLogEntry.hash] = entry.textHash
    }
}

private fun ResultRow.toAccessLogEntry(): AccessLogEntry {
    val ipAddress = this[DbAccessLogEntry.ipAddress]
    val accessTimeMillis = this[DbAccessLogEntry.accessTimeMillis]
    val requestPath = this[DbAccessLogEntry.requestPath]
    val responseCode = this[DbAccessLogEntry.responseCode]
    val bytesSent = this[DbAccessLogEntry.bytesSent]
    val referer = this[DbAccessLogEntry.referrer]
    val userAgent = this[DbAccessLogEntry.userAgent]
    val hidden = this[DbAccessLogEntry.hidden]
    val hash = this[DbAccessLogEntry.hash]
    return AccessLogEntry(
        ipAddress = ipAddress,
        accessTimeMillis = accessTimeMillis,
        requestPath = requestPath,
        responseCode = responseCode,
        bytesSent = bytesSent,
        referrer = referer,
        userAgent = userAgent,
        hidden = hidden,
        textHash = hash
    )
}