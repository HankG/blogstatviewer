package me.hankg.blogstatviewer.hashing

import com.google.common.hash.Hashing
import java.nio.charset.StandardCharsets

/**
 * Generates SHA-256 hashes using JVM calls
 */
@Suppress("UnstableApiUsage")
actual object StringHashGenerator {
    actual fun hashString(string: String): String {
        return Hashing.sha256().hashString(string, StandardCharsets.UTF_16).toString()
    }
}
