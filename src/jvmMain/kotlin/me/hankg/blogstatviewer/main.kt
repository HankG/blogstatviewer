package me.hankg.blogstatviewer

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.flag
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.types.file
import com.github.ajalt.clikt.parameters.types.int
import com.github.ajalt.clikt.parameters.types.enum
import kotlinx.datetime.Instant
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import me.hankg.blogstatviewer.analysis.LinkStatGenerator
import me.hankg.blogstatviewer.analysis.SuspiciousIpAnalysis
import me.hankg.blogstatviewer.data.AccessLogRepositoryFactory
import me.hankg.blogstatviewer.filtering.checkIsOkay
import me.hankg.blogstatviewer.model.*
import me.hankg.blogstatviewer.parsers.NginxDefaultAccessLineParser
import me.hankg.blogstatviewer.parsers.TextLogParser
import mu.KotlinLogging
import java.nio.file.Files
import java.nio.file.Paths
import kotlin.streams.toList
import kotlin.text.StringBuilder

const val DEFAULT_BATCH_INSERT_SIZE = 100

private val logger = KotlinLogging.logger("BlogStatViewerMain")

class RunAnalysis : CliktCommand() {
    // CLI Arguments
    private val batchInsertSize: Int by option(
        "-b",
        "--batch",
        help = "Size of each batch insert to database (default = $DEFAULT_BATCH_INSERT_SIZE)"
    )
        .int()
        .default(DEFAULT_BATCH_INSERT_SIZE)

    private val ingestLogs by option("-i", "--ingest", help = "Whether to try to ingest new log data").flag()
    private val summarize by option("-s", "--summarize", help = "Whether to try to ingest new log data").flag()
    private val findBadIps by option("--find-badip", help = "Look for potentially bad IPs in the data").flag()
    private val listForIp by option("--list-ip", help = "List all entries for the IP address")
    private val linkTimeline by option("-l", "--link-timeline", help = "Create timeline for link access counts")
    private val linkTimelineBatchSize by option("-t", "--timeline-batch-type", help="Timeline Batch Type")
        .enum<TimeBinSize>()
        .default(TimeBinSize.Month)
    private val reapplyFilters by option(
        "-f",
        "--apply-filters",
        help = "Applies the current filter configuration to the existing dataset, toggling hidden field accordingly not deleting data")
        .flag()

    private val settingsFile by argument(help = "Location of run settings configuration")
        .file(mustExist = true, mustBeReadable = true, canBeDir = false, canBeFile = true)

    // Common settings and repo objects

    override fun run() {
        logger.info { "Blog Stat Viewer" }

        if(linkTimeline?.isNotEmpty()?:false) {
            outputLinkTimeline(linkTimeline!!, linkTimelineBatchSize)
            return;
        }

        if (ingestLogs) {
            processNewLogEntries()
        }

        if (summarize) {
            regenerateStatistics()
        }

        if (findBadIps) {
            findBadIps()
        }

        if (reapplyFilters) {
            reapplyFilters()
        }

        if (listForIp?.isNotEmpty()?:false) {
            listForIp(listForIp!!)
        }

        logger.info { "COMPLETE" }
    }

    private fun outputLinkTimeline(link:String, binSize: TimeBinSize) {
        val settings = Json.decodeFromString<RunSettings>(Files.readString(settingsFile.toPath()))
        val externalReferrerSettings =
            Json.decodeFromString<ReferrerMappingSettings>(Files.readString(Paths.get(settings.referrerMappingSettings)))
        val internalReferrerSettings =
            Json.decodeFromString<InternalReferrerSettings>(Files.readString(Paths.get(settings.internalMappingSettings)))
        val repo = AccessLogRepositoryFactory.getSqliteDbFromFilePath(settings.dbPath)

        logger.info { "Generating Timeline for link: $link" }
        val entries = repo.findAll(true).filter{ it.requestPath.startsWith(link)}.sortedBy { it.accessTimeMillis }
        val timeBins = LinkStatGenerator(externalReferrerSettings, internalReferrerSettings)
            .generateByTimeBuckets(entries, binSize)
        timeBins.forEach {
            val summary = LinkStatSummary.summarize(it)
            println("${it.date}, ${summary.viewCount}")
        }


    }

    private fun processNewLogEntries() {
        logger.info { "Process new log entries" }
        val settings = Json.decodeFromString<RunSettings>(Files.readString(settingsFile.toPath()))
        val filterConfig = Json.decodeFromString<FilterSettings>(Files.readString(Paths.get(settings.filterSettings)))
        val repo = AccessLogRepositoryFactory.getSqliteDbFromFilePath(settings.dbPath)

        val parser = TextLogParser(NginxDefaultAccessLineParser())

        logger.info { "Loading filter settings from: ${settings.filterSettings}" }
        val filterCriteria = filterConfig.toFilterCriteriaList()

        val filesToProcess = Files.walk(Paths.get(settings.logFolderPath))
            .filter {
                val nameString = it.fileName.toString()
                nameString.startsWith("access") && !nameString.endsWith(".gz")
            }
            .toList()

        logger.info { "# Files to process: ${filesToProcess.size}" }

        val allLogEntries = filesToProcess
            .flatMap {
                logger.info { "Processing $it" }
                parser.process(it.toString())
            }.toList()

        logger.info { "Filtering entries" }
        val filteredEntries = allLogEntries.parallelStream()
            .filter { it.checkIsOkay(filterCriteria) }
            .toList()
        logger.info { "All read log entries:       ${allLogEntries.size}" }
        logger.info { "Total relevant log entries: ${filteredEntries.size}" }

        logger.info { "Inserting into Database" }
        val originalCount = repo.countAll()
        filteredEntries.chunked(batchInsertSize)
            .forEach {
                repo.insertMany(it)
            }
        val newInserts = repo.countAll() - originalCount

        logger.info { "New log entries inserted:   $newInserts" }

    }

    private fun regenerateStatistics() {
        logger.info { "Generating statistics" }
        val settings = Json.decodeFromString<RunSettings>(Files.readString(settingsFile.toPath()))
        val externalReferrerSettings =
            Json.decodeFromString<ReferrerMappingSettings>(Files.readString(Paths.get(settings.referrerMappingSettings)))
        val internalReferrerSettings =
            Json.decodeFromString<InternalReferrerSettings>(Files.readString(Paths.get(settings.internalMappingSettings)))
        val repo = AccessLogRepositoryFactory.getSqliteDbFromFilePath(settings.dbPath)


        val outputPath = Paths.get(settings.summaryOutput)
        val output = StringBuilder()

        val entries = repo.findAll(true)
        val monthlyBinnedEntries = LinkStatGenerator(externalReferrerSettings, internalReferrerSettings)
            .generateByTimeBuckets(entries, TimeBinSize.Month)
        val annualBinnedEntries = LinkStatGenerator(externalReferrerSettings, internalReferrerSettings)
            .generateByTimeBuckets(entries, TimeBinSize.Year)

        val monthlySummary = monthlyBinnedEntries.associate { it.date to LinkStatSummary.summarize(it) }
        val annualSummary = annualBinnedEntries.associate { it.date.year to LinkStatSummary.summarize(it) }

        var lastYear = 0
        val maxEntries = 10
        monthlySummary.toSortedMap().forEach { (date, summary) ->
            val year = date.year
            if (year != lastYear) {
                output.appendLine()
                output.appendLine("============================================================================================")
                output.appendLine("$year Summary")
                writeSummaryStats(output, annualSummary[year] ?: LinkStatSummary(), maxEntries, false)
                lastYear = year
            }
            output.appendLine("${date.month} = ${summary.viewCount}")
        }

        output.appendLine("============================================================================================")
        output.appendLine("============================================================================================")
        output.appendLine()
        output.appendLine("Monthly details: ")
        monthlyBinnedEntries.forEach {
            output.appendLine("------------------------------------------------------------------------------------------")
            output.appendLine("${it.date.year}-${it.date.monthNumber} Monthly Data")
            writeSummaryStats(output, LinkStatSummary.summarize(it), maxEntries, true)
            output.appendLine()
        }

        logger.info { "Writing summary output to $outputPath" }
        Files.writeString(outputPath, output.toString())
    }

    private fun writeSummaryStats(output:java.lang.StringBuilder, summary: LinkStatSummary, maxEntries:Int, withTopIps:Boolean) {
        with(summary) {
            output.appendLine("View count: ${this.viewCount}")
            output.appendLine("Unique IPs: ${this.ipStats.size}")
            if(withTopIps) {
                output.appendLine()
                output.appendLine("Top $maxEntries IPs: ")
                this.ipStats
                    .map { Pair(it.key, it.value) }
                    .sortedByDescending { it.second }
                    .take(maxEntries)
                    .forEach { output.appendLine("${it.first} = ${it.second}") }
            }

            output.appendLine()
            output.appendLine("Top $maxEntries Links: ")
            this.linkViews
                .map { Pair(it.key, it.value) }
                .sortedByDescending { it.second }
                .take(maxEntries)
                .forEach { output.appendLine("${it.first} = ${it.second}") }

            output.appendLine()
            output.appendLine("Top $maxEntries Referrers: ")
            this.externalReferrers
                .map { Pair(it.key, it.value) }
                .sortedByDescending { it.second }
                .take(maxEntries)
                .forEach { output.appendLine("${it.first} = ${it.second}") }

            output.appendLine()
            output.appendLine("Top $maxEntries Internal Referrers: ")
            this.internalReferrers
                .map { Pair(it.key, it.value) }
                .sortedByDescending { it.second }
                .take(maxEntries)
                .forEach { output.appendLine("${it.first} = ${it.second}") }

            output.appendLine()
            output.appendLine("Browser statistics:")
            this.browserNameStats
                .map { Pair(it.key, it.value) }
                .sortedByDescending { it.second }
                .take(maxEntries)
                .forEach { output.appendLine("${it.first} = ${it.second}") }

            output.appendLine()
            output.appendLine("OS statistics:")
            this.osNameStats
                .map { Pair(it.key, it.value) }
                .sortedByDescending { it.second }
                .take(maxEntries)
                .forEach { output.appendLine("${it.first} = ${it.second}") }
        }
    }

    private fun findBadIps() {
        val settings = Json.decodeFromString<RunSettings>(Files.readString(settingsFile.toPath()))
        val repo = AccessLogRepositoryFactory.getSqliteDbFromFilePath(settings.dbPath)

        logger.info { "Performing Suspicious IP Analysis of dataset" }
        val entries = repo.findAll(true)
        logger.info { "Studying ${entries.size} entries" }
        val badIpSettings = settings.badIpStudySettings
        val analyzer = SuspiciousIpAnalysis(badIpSettings.windowSizeInMillis, badIpSettings.maxPerWindow)
        val badIps = analyzer.findIps(entries)
        if (badIps.isEmpty()) {
            logger.info { "No suspicious IPs found" }
        } else {
            logger.info { "Flagged potentiall suspicious IPs: ${badIps}" }
        }

    }

    private fun reapplyFilters() {
        logger.info { "Updating entries with new filter" }
        val settings = Json.decodeFromString<RunSettings>(Files.readString(settingsFile.toPath()))
        val filterConfig = Json.decodeFromString<FilterSettings>(Files.readString(Paths.get(settings.filterSettings)))
        val filterCriteria = filterConfig.toFilterCriteriaList()
        val repo = AccessLogRepositoryFactory.getSqliteDbFromFilePath(settings.dbPath)

        val entries = repo.findAll(false)

        val updatedEntries = entries.map {
            val shouldBeHidden = !it.checkIsOkay(filterCriteria)
            val changed = it.hidden != shouldBeHidden
            Pair(changed, it.copy(hidden = shouldBeHidden))
        }.filter { it.first }
            .map { it.second }

        logger.info { "Entries in database: ${entries.size}" }
        logger.info { "Entries for updating: ${updatedEntries.size}"}
        val successfulUpdateCount = updatedEntries.map { repo.update(it) }.filter { it }.size
        logger.info { "Successfully updated: $successfulUpdateCount"}

        logger.info { "Update complete" }
    }

    private fun listForIp(ip: String) {
        val settings = Json.decodeFromString<RunSettings>(Files.readString(settingsFile.toPath()))
        val repo = AccessLogRepositoryFactory.getSqliteDbFromFilePath(settings.dbPath)

        logger.info { "Performing Suspicious IP Analysis of dataset" }
        val entries = repo.findByIp(ip).sortedBy { it.accessTimeMillis }
        entries.forEach {
            val entryString = """
                IP Address   = ${it.ipAddress}
                Access Time  = ${Instant.fromEpochMilliseconds(it.accessTimeMillis)},
                Request Path = ${it.requestPath}
                Referrer     = ${it.referrer}
                User Agent   = ${it.userAgent}
                Hidden       = ${it.hidden}
                
            """.trimIndent()
            logger.info { entryString }
        }

    }

}

fun main(args: Array<String>) = RunAnalysis().main(args)