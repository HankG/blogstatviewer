package me.hankg.blogstatviewer.parsers

import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter

actual object DateTimeParser {

    /**
     * Given a date string and the format of that string it converts it into the number of
     * milliseconds since 1/1/1970 00:00:00 as local date/time. All time zone information
     * will be ignored so local date/time strings should be passed in and used.
     */
    actual fun toUnixTime(dateString: String, format: String): Long {
        return LocalDateTime.parse(dateString, DateTimeFormatter.ofPattern(format))
            .atOffset(ZoneOffset.UTC)
            .toInstant()
            .toEpochMilli()
    }

    /**
     * Given the Unix timestamp in milliseconds generate an ISO Date/Time string. It is
     * assuming all local date/time calculations therefore there should be no adjustment
     * for timezone changes.
     */
    actual fun toISOString(unixTimeMillis: Long): String {
        return Instant.ofEpochMilli(unixTimeMillis)
            .atOffset(ZoneOffset.UTC)
            .toLocalDateTime()
            .format(DateTimeFormatter.ISO_DATE_TIME)
    }
}