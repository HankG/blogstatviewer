package me.hankg.blogstatviewer.data

import java.nio.file.Files
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import kotlin.test.fail

class AccessLogRepositoryFactoryTest {

    @Test
    fun testGetSqlLiteDb() {
        val path = Files.createTempFile("scratch", ".db").toString()
        val connectionString = AccessLogRepositoryFactory.generateSqliteConnectionString(path)
        assertTrue(AccessLogRepositoryFactory.getSqliteDatabase(connectionString) is JvmSqlLiteAccessLogRepository)
    }


    @Test
    fun testGetSqlLiteDbFromfilepath() {
        val path = Files.createTempFile("scratch", ".db").toString()
        assertTrue(AccessLogRepositoryFactory.getSqliteDbFromFilePath(path) is JvmSqlLiteAccessLogRepository)
    }

    @Test
    fun testGetSqliteConnectionString() {
        val path = "/tmp/mydata.db"
        val expectedConnectionString = "jdbc:sqlite:$path"
        val actualConnectionString = AccessLogRepositoryFactory.generateSqliteConnectionString(path)
        assertEquals(expectedConnectionString, actualConnectionString)
    }
}