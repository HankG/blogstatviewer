package me.hankg.blogstatviewer.data

import me.hankg.blogstatviewer.model.AccessLogEntry
import java.nio.file.Files
import java.util.*
import kotlin.random.Random
import kotlin.test.*

class JvmSqlLiteAccessLogRepositoryTest {
    val dbPath = Files.createTempFile("accesslogdb", ".db").toString()
    val connectionString = AccessLogRepositoryFactory.generateSqliteConnectionString(dbPath)
    val repo = JvmSqlLiteAccessLogRepository(connectionString)

    @Test
    fun testCreation() {
        assertTrue(repo.findAll(false).isEmpty())
    }

    @Test
    fun testInsertionAndQuery() {
        val newEntry = randomEntry()
        assertTrue(repo.insert(newEntry))
        val results = repo.findAll(false)
        assertEquals(1, results.size)
        assertEquals(newEntry, results.first())
        val newEntry2 = randomEntry()
        assertTrue(repo.insert(newEntry2))
        val newResults = repo.findAll(false)
        assertEquals(2, newResults.size)
        assertEquals(newEntry2, newResults.last())
    }

    @Test
    fun testFindAllExcludeHidden() {
        val newEntry = randomEntry()
        assertTrue(repo.insert(randomEntry().copy(hidden = true)))
        assertTrue(repo.insert(newEntry))
        assertTrue(repo.insert(randomEntry().copy(hidden = true)))
        assertTrue(repo.insert(randomEntry().copy(hidden = true)))
        val results = repo.findAll(true)
        assertEquals(listOf(newEntry), results)
    }

    @Test
    fun testFindByIp() {
        val desiredIp = "2"
        val expectedCount = 1
        val entries = listOf(
            randomEntry().copy(ipAddress = "1"),
            randomEntry().copy(ipAddress = desiredIp),
            randomEntry().copy(ipAddress = "3"),
        )
        repo.insertMany(entries)
        val pulled = repo.findByIp(desiredIp)
        assertEquals(expectedCount, pulled.size)
        assertEquals(desiredIp, pulled.first().ipAddress)
    }

    @Test
    fun testCount() {
        val size = 10L
        assertEquals(0, repo.countAll())
        assertTrue(repo.insertMany((1..size).map { randomEntry() }))
        assertEquals(size, repo.countAll())
    }


    @Test
    fun testInsertMany() {
        val numberEntries = 10
        val entries = (1..numberEntries).map { randomEntry() }
        assertTrue(repo.insertMany(entries))
        val queriedEntries = repo.findAll(false)
        assertEquals(entries, queriedEntries)
    }

    @Test
    fun testInsertManyWithDuplicates() {
        val uniqueEntries = 10
        val expectedTotalEntries = 2 * uniqueEntries
        val baseEntries = (1..uniqueEntries).map { randomEntry() }
        val entries = baseEntries + baseEntries
        assertEquals(expectedTotalEntries, entries.size)
        assertTrue(repo.insertMany(entries))
        val queriedEntries = repo.findAll(false)
        assertEquals(baseEntries, queriedEntries)
    }


    @Test
    fun testInsertManyWithExistingItemDuplicates() {
        val uniqueEntries = 10
        val batch1 = (1..uniqueEntries).map { randomEntry() }
        val batch2 = (1..uniqueEntries).map { randomEntry() }
        val batch3 = listOf(batch1.first(), batch2.first(), randomEntry(), randomEntry())
        val expectedTotal = batch1.size + batch2.size + (batch3.size - 2)
        assertTrue(repo.insertMany(batch1))
        assertTrue(repo.insertMany(batch2))
        assertTrue(repo.insertMany(batch3))
        assertEquals(expectedTotal.toLong(), repo.countAll())
    }


    @Test
    fun testInsertDuplicateItem() {
        val newEntry = randomEntry()
        assertTrue(repo.insert(newEntry))
        assertFalse(repo.insert(newEntry))
        assertFalse(repo.insert(newEntry))
        val results = repo.findAll(false)
        assertEquals(1, results.size)
        assertEquals(newEntry, results.first())
    }

    @Test
    fun testUpdateUninsertedItem() {
        val newEntry = randomEntry()
        assertFalse(repo.update(newEntry))
        assertTrue(repo.findAll(false).isEmpty())
    }

    @Test
    fun testUpdateItem() {
        val newEntry = randomEntry()
        assertTrue(repo.insert(newEntry))
        assertEquals(1, repo.findAll(false).size)
        assertEquals(newEntry, repo.findAll(false).first())

        val updatedEntry = randomEntry().copy(textHash = newEntry.textHash)
        assertTrue(repo.update(updatedEntry))
        assertEquals(1, repo.findAll(false).size)
        assertEquals(updatedEntry, repo.findAll(false).first())
    }

    private fun randomEntry(): AccessLogEntry {
        return AccessLogEntry(
            ipAddress = UUID.randomUUID().toString(),
            accessTimeMillis = System.currentTimeMillis(),
            requestPath = UUID.randomUUID().toString(),
            responseCode = Random.nextInt(),
            bytesSent = Random.nextLong(),
            referrer = UUID.randomUUID().toString(),
            userAgent = UUID.randomUUID().toString(),
            textHash = UUID.randomUUID().toString()
        )
    }
}