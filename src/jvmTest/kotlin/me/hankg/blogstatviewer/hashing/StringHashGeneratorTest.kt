package me.hankg.blogstatviewer.hashing

import kotlin.test.Test
import kotlin.test.assertEquals

class StringHashGeneratorTest {

    @Test
    fun testUtf8StringHashing() {
        val message = "This string should hash"
        val expectedHash = "9d68ba2674e79498889df1d094258bc7fa295792c4e8b3fa852c7d9866ddc15a"
        val actualHash = StringHashGenerator.hashString(message)
        assertEquals(expectedHash, actualHash)
    }
}