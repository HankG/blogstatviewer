package me.hankg.blogstatviewer.parsers

import kotlin.test.Test
import kotlin.test.assertEquals

class DateTimeParserTest {

    @Test
    fun testUnixEpochToMilliseconds() {
        val dateString = "01/01/1970 00:00:00"
        val dateFormat = "MM/dd/yyyy HH:mm:ss"
        val expectedEpoch = 0L
        val actualEpoch = DateTimeParser.toUnixTime(dateString, dateFormat)
        assertEquals(expectedEpoch, actualEpoch)
    }

    @Test
    fun testSampleNginxDefaultDateFormat() {
        val dateString = "25/Feb/2021:06:39:04"
        val dateFormat = "d/MMM/yyyy:HH:mm:ss"
        val expectedEpoch = 1614235144000L
        val actualEpoch = DateTimeParser.toUnixTime(dateString, dateFormat)
        assertEquals(expectedEpoch, actualEpoch)
    }

    @Test
    fun testSampleNginxDefaultDateFormat2() {
        val dateString = "3/Feb/2021:06:39:04"
        val dateFormat = "d/MMM/yyyy:HH:mm:ss"
        val expectedEpoch = 1612334344000L
        val actualEpoch = DateTimeParser.toUnixTime(dateString, dateFormat)
        assertEquals(expectedEpoch, actualEpoch)
    }

    @Test
    fun testUnixEpochToISOString() {
        val expected = "1970-01-01T00:00:00"
        val actual = DateTimeParser.toISOString(0)
        assertEquals(expected, actual)
    }

    @Test
    fun testToISOString() {
        val expected = "2021-02-03T06:39:04"
        val actual = DateTimeParser.toISOString(1612334344000L)
        assertEquals(expected, actual)
    }
}