package me.hankg.blogstatviewer.parsers

import me.hankg.blogstatviewer.model.AccessLogEntry
import me.hankg.blogstatviewer.model.BrowserData
import me.hankg.blogstatviewer.model.OperatingSystemData
import me.hankg.blogstatviewer.model.PlatformData
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

class PlatformParserTest {
    private val parser = PlatformParser()

    @Test
    fun testChrome() {
        val agent =
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.111 Safari/537.36"
        checkBrowser(BrowserData("Chrome", "47.0.2526.111"), parser.getPlatform(accessEntryFromAgentString(agent)))
    }

    @Test
    fun testSafari() {
        val agent =
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/600.1.25 (KHTML, like Gecko) Version/8.0 Safari/600.1.25"
        checkBrowser(BrowserData("Safari", "600.1.25"), parser.getPlatform(accessEntryFromAgentString(agent)))

    }

    @Test
    fun testOpera() {
        val agent =
            "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36 OPR/74.0.3911.107"
        checkBrowser(BrowserData("OPR", "74.0.3911.107"), parser.getPlatform(accessEntryFromAgentString(agent)))
    }

    @Test
    fun testFirefox() {
        val agent = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/65.0"
        checkBrowser(BrowserData("Firefox", "65.0"), parser.getPlatform(accessEntryFromAgentString(agent)))
    }

    @Test
    fun testLinux() {
        val agent1 = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/65.0"
        checkOperatingSystem(OperatingSystemData("Linux"), parser.getPlatform(accessEntryFromAgentString(agent1)))

        val agent2 = "Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/66.0"
        checkOperatingSystem(OperatingSystemData("Linux"), parser.getPlatform(accessEntryFromAgentString(agent2)))

        val agent3 = "Mozilla/5.0 (X11; Linux i686; rv:78.0) Gecko/20100101 Firefox/78.0"
        checkOperatingSystem(OperatingSystemData("Linux"), parser.getPlatform(accessEntryFromAgentString(agent3)))
    }

    @Test
    fun testBSD() {
        val agent1 = "Mozilla/5.0 (X11; U; OpenBSD i386; en-US; rv:1.8.0.7) Gecko/20060920 Firefox/52.0.1"
        checkOperatingSystem(OperatingSystemData("OpenBSD"), parser.getPlatform(accessEntryFromAgentString(agent1)))
    }


    @Test
    fun testMac() {
        val agent1 =
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3393.4 Safari/537.36"
        checkOperatingSystem(
            OperatingSystemData("Macintosh", "10.12.3"),
            parser.getPlatform(accessEntryFromAgentString(agent1))
        )

        val agent2 = "Mozilla/5.0 (Macintosh; Intel Mac OS X 11.1; rv:84.0) Gecko/20100101 Firefox/84.0"
        checkOperatingSystem(
            OperatingSystemData("Macintosh", "11.1"),
            parser.getPlatform(accessEntryFromAgentString(agent2))
        )
    }

    @Test
    fun testWindows() {
        val agent1 =
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36"
        checkOperatingSystem(
            OperatingSystemData("Windows", "10.0"),
            parser.getPlatform(accessEntryFromAgentString(agent1))
        )

        val agent2 = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.6) Gecko/20070725 Firefox/2.0.0.6"
        checkOperatingSystem(
            OperatingSystemData("Windows", "5.1"),
            parser.getPlatform(accessEntryFromAgentString(agent2))
        )

        val agent3 =
            "User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.64 Safari/537.31"
        checkOperatingSystem(
            OperatingSystemData("Windows", "6.1"),
            parser.getPlatform(accessEntryFromAgentString(agent3))
        )
    }

    @Test
    fun testAndroid() {
        val agent1 =
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36"
        checkOperatingSystem(
            OperatingSystemData("Windows", "10.0"),
            parser.getPlatform(accessEntryFromAgentString(agent1))
        )

        val agent2 = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.6) Gecko/20070725 Firefox/2.0.0.6"
        checkOperatingSystem(
            OperatingSystemData("Windows", "5.1"),
            parser.getPlatform(accessEntryFromAgentString(agent2))
        )
    }

    @Test
    fun testIOS() {
        val agent1 =
            "Mozilla/5.0 (iPad; CPU OS 10_3_3 like Mac OS X) AppleWebKit/603.3.8 (KHTML, like Gecko) Version/10.0 Mobile/14G60 Safari/602.1"
        checkOperatingSystem(
            OperatingSystemData("iOS", "10.3.3"),
            parser.getPlatform(accessEntryFromAgentString(agent1))
        )

        val agent2 =
            "Mozilla/5.0 (iPhone; CPU iPhone OS 12_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 [FBAN/FBIOS;FBDV/iPhone10,2;FBMD/iPhone;FBSN/iOS;FBSV/12.2;FBSS/3;FBCR/Voda NZ;FBID/phone;FBLC/en_GB;FBOP/5]"
        checkOperatingSystem(
            OperatingSystemData("iOS", "12.2"),
            parser.getPlatform(accessEntryFromAgentString(agent2))
        )

        val agent3 =
            "Mozilla/5.0 (iPhone; CPU iPhone OS 12_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 [LinkedInApp]"
        checkOperatingSystem(
            OperatingSystemData("iOS", "12.2"),
            parser.getPlatform(accessEntryFromAgentString(agent3))
        )

    }

    @Test
    fun testChromeOS() {
        val agent =
            "Mozilla/5.0 (X11; CrOS aarch64 13597.84.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.153 Safari/537.36"
        checkOperatingSystem(OperatingSystemData("CrOS"), parser.getPlatform(accessEntryFromAgentString(agent)))
    }

    @Test
    fun testEmptyValue() {
        checkFull("", PlatformData())
    }

    @Test
    fun testNoneValue() {
        checkFull(AccessLogEntry.NO_AGENT, PlatformData())
    }

    @Test
    fun testMalformed() {
        val agent = "hackney/1.14.3"
        checkFull(agent, PlatformData())
    }

    @Test
    fun testOldIEBrowser() {
        val agent = "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0;  Trident/5.0)"
        checkFull(agent, PlatformData(operatingSystem = OperatingSystemData("Windows", "6.1")))
    }

    @Test
    fun testFakeUnknownOSAndBrowser() {
        val agent = "Mozilla/5.0 (MyOS; MiOhEss 10.3) SuperJSEngine/17.5 MiOhEssBrowser/22.7.3"
        checkFull(
            agent,
            PlatformData(BrowserData("MiOhEssBrowser", "22.7.3"), OperatingSystemData("MyOS; MiOhEss 10.3"))
        )
    }

    private fun accessEntryFromAgentString(agent: String): AccessLogEntry = AccessLogEntry(userAgent = agent)

    private fun checkFull(agent: String, expected: PlatformData) {
        val entry = accessEntryFromAgentString(agent)
        assertEquals(expected, parser.getPlatform(entry))
    }

    private fun checkBrowser(expected: BrowserData, data: PlatformData) {
        assertEquals(expected, data.browser)
    }

    private fun checkOperatingSystem(expected: OperatingSystemData, data: PlatformData) {
        assertEquals(expected, data.operatingSystem)
    }
}