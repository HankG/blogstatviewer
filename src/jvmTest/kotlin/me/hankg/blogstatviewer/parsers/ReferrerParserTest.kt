package me.hankg.blogstatviewer.parsers

import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import me.hankg.blogstatviewer.model.AccessLogEntry
import me.hankg.blogstatviewer.model.ReferrerMappingSettings
import java.nio.file.Files
import java.nio.file.Paths
import kotlin.test.Test
import kotlin.test.assertEquals

class ReferrerParserTest {
    private val mappingsFile = Paths.get("src/commonTest/resources/referrerMappingSettings.json")
    private val mappings = Json.decodeFromString<ReferrerMappingSettings>(Files.readString(mappingsFile))
    private val parser = ReferrerParser(mappings)

    @Test
    fun testDuckDuckGoStrings() {
        val inputStrings = listOf(
            AccessLogEntry(referrer = "https://duckduckgo.com/"),
        )
        evaluate(inputStrings, ReferrerResult("DuckDuckGo", true))
    }

    @Test
    fun testFacebookStrings() {
        val inputStrings = listOf(
            AccessLogEntry(referrer = "https://m.facebook.com/"),
        )
        evaluate(inputStrings, ReferrerResult("Facebook", true))
    }

    @Test
    fun testGoogleStrings() {
        val entries = listOf(
            AccessLogEntry(referrer = "https://www.google.com/"),
            AccessLogEntry(referrer = "https://www.google.ru/"),
            AccessLogEntry(referrer = "https://www.google.co.th/"),
            AccessLogEntry(requestPath = "/2020/10/20/article?gclid=12345")
        )

        evaluate(entries, ReferrerResult("Google", true))
    }

    @Test
    fun testUnknown() {
        evaluate(
            listOf(AccessLogEntry(referrer = "https://newsearchengine.com/")),
            ReferrerResult("https://newsearchengine.com/", false)
        )
        evaluate(
            listOf(AccessLogEntry(referrer = "https://someblog.org/")),
            ReferrerResult("https://someblog.org/", false)
        )
    }

    private fun evaluate(inputs: List<AccessLogEntry>, expected: ReferrerResult) {
        inputs.forEach {
            assertEquals(expected, parser.convertToType(it), "Bad conversion: $it")
        }
    }
}